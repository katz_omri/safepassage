<?php

namespace App\Policies;



use App\Models\UserLoginData as User;
use App\Models\Role;

class RolePolicy
{
    function __construct()
    {

    }

    /**
     * @param User $user
     * @param Role $role
     * @param null $userId
     * @return bool
     */
    public function show(User $user, $role = null, $userId = null)
    {
        return ($userId == $user->users->first()->id);
    }


    /**
     * Determine if the given post can be updated by the user.
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function index()
    {

//         foreach($this->getPermissions() as $permission) {
//
//            $gate->define($permission->name, function($user) {
//
//                $user->hasRole($permission->name);
//            });
//        }
    }
}
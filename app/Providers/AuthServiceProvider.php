<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\Ruser;
use App\Models\Sponsor;
use App\Policies\RolePolicy;
use App\Policies\SponsorPolicy;
use App\Policies\UserPolicy;
use App\Models\User as User;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
    ];
    protected $auth;

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @param Guard $auth
     * @return array
     * @internal param User $user
     */
    public function boot(GateContract $gate, Guard $auth)
    {

        $gate->before(function ($user, $ability) {
            if ($user->isAdmin()) {
                return true;
            }
        });

        if(app()->runningInConsole())
        {
            return [];
        }
        $this->registerPolicies($gate);
        $gate->define('sponsor-own', function ($user, $resourceId) {
            return $user->user->sponsor->id == $resourceId;
        });
        $gate->define('owner', function ($loginData, $resourceId) {
            return $loginData->user->id == $resourceId;
        });
        foreach($this->getPermissions() as $permission) {
            $gate->define($permission->name, function($user) use ($permission) {
                if (is_null($user)) {
                    return false;
                }
                return $user->users->hasRole($permission->roles);
            });
        }

    }

    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}

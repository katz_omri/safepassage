<?php namespace App\Providers;


use Fractal;
use Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider {

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('item', function($data, $transformer, $metadata = null)
        {
            return Fractal::item($data, new $transformer(), $metadata);
        });
        Response::macro('collection', function($data, $transformer, $metadata = null)
        {
            return Fractal::collection($data, new $transformer(), $metadata);
        });

        Response::macro('paginatedCollection', function($data, Transformable $transformer, $metadata = null)
        {
            return Fractal::paginatedCollection($data, new $transformer(), $metadata);
        });
    }

    public function register(){}

}
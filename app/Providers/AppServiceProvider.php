<?php namespace App\Providers;


use App\Models\Exception;

use Blade;
use Sorskod\Larasponse\Providers\Fractal as Manager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{

	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{

        Blade::setContentTags('<%', '%>');        // for variables and all things Blade
        Blade::setEscapedContentTags('<%%', '%%>');   // for escaped data
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
        $this->app->bind('App\Http\Validators\FactoryMethod', 'App\Http\Validators\ValidatorFactory');
        $this->app->bind('exceptions', function()
        {
            return new Exception();
        });
        $this->app->singleton('fractal', function($app) { return new Manager(
//            $app['League\Fractal\Serializer\ArraySerializer'],
//            $app['App\Serializers\ResponseSerializer'],
            $app['League\Fractal\Serializer\JsonApiSerializer'],
            $app['Illuminate\Http\Request']
        );});

        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }

        $this->app->when('App\Http\Controllers\SponsorController')
            ->needs('App\Contracts\ValidatorContract')
            ->give('App\Http\Validators\SponsorValidator');
	}

}

<?php namespace App\Contracts;
use Illuminate\Http\Request;


/**
 * Created by PhpStorm.
 * User: omri
 * Date: 11/28/15
 * Time: 18:08
 */


interface UserContract
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    function enableLogin($request);

    /**
     * @return mixed
     */
    function disableLogin();

    /**
     * @return mixed
     */
    function isActive();


    /**
     * @return mixed
     */
    function sponsor();


    function userType();
    //

    /**
     * @return mixed -
     * reverse the order of roles, this will return the strongest role always.
     */
    function getRoles();

}

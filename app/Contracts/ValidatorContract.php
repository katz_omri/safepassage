<?php namespace App\Contracts;


interface ValidatorContract
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    static function rules();


    /**
     * Get the validation rules that apply to the request.
     *
     *
     * @param $request
     * @return \Illuminate\Validation\Validator
     */

    public function make();

    public function setRequest($request);

}
<?php namespace App\Contracts;

/**
 * Interface Transformable
 * @package App\Contracts
 */
interface Transformable {

    /**
     * Transform the data
     *
     * @return mixed
     * @internal param $i_Collection
     * @internal param Transformable $transformable
     */
    public function transformer();


}

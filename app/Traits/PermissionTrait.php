<?php namespace App\Traits;


use Illuminate\Support\Facades\Config;

trait PermissionTrait
{

    public function roles()
    {
        return $this->belongsToMany(Config::get('acl.role'), Config::get('acl.permission_role_table'));
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function($permission) {
            if (!method_exists(Config::get('acl.permission'), 'bootSoftDeletingTrait')) {
                $permission->roles()->sync([]);
            }

            return true;
        });
    }
}

<?php

namespace App\Jobs;

use App\Http\Forms\SendMailForm;
use App\Models\Mail;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContactUsMail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $mail;

    /**
     * Create a new job instance.
     *
     * @param Mail $mail
     */
    public function __construct(Mail $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {

        $this->mail->status = Mail::IN_PROGRESS;
        $this->mail->setRecipients(['info@safepassage.ca', 'omrikatz12@gmail.com'])
            ->send();


    }
}

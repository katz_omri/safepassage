<?php namespace App\Enums;

interface MailStatus extends BaseEnumContract
{
    const NOT_STARTED = 0;
    const IN_PROGRESS = 1;
    const SENT = 2;
    const FAILED = 3;
}
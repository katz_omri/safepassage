<?php namespace App\Services;

use App\Repositories\Eloquent\SponsorRepository as Sponsor;
use App\Repositories\Eloquent\UserRepository as User;
use Validator;
use DB;
use App\Repositories\Eloquent\UserLoginDataRepository as UserLoginData;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {


    function __construct(User $user, UserLoginData $loginData, Sponsor $sponsor)
    {
        $this->loginData = $loginData;
        $this->sponsor = $sponsor;
        $this->user = $user;
    }

    /**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
     * TODO:: Need to add all validation required to add a new user.
	 */
	public function validator(array $data)
	{
		return array(
            Validator::make($data, [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:user_login_data,login_email',
                'password' => 'required|confirmed|min:6',
		    ]),
        );
	}

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     * TODO:: Need to add all user fields when creating a new user
     * First creates a sponsor account.
     * Then create a first user account to be used to contain the specific user for that sponsor.
     * Then we insert login_data with password to the user_login_data table.
     *
     */
    public function create(array $data)
    {
        return DB::transaction(function() use ($data) {
            $sponsor = $this->sponsor->create([
                'email' => $data['email'],
                'first_name' => $data['name']
            ]);
            $user = $sponsor->users()->save(
                $this->user->create([
                    'first_name' => $data['name'],
                    'email' => $data['email'],
                ])
            );


            $user->loginData()->associate(
                $this->loginData->create([
                    'login_email' => $data['email'],
                    'password' => bcrypt($data['password'])
                ])
            )->save();

//            dd($user);
//            $user->loginData()->create([
//                'login_email' => $data['email'],
//                'password' => bcrypt($data['password'])
//            ]);
//            $this->loginData->create([
//                'account_id' => $user->id,
//                'login_email' => $data['email'],
//                'password' => bcrypt($data['password'])
//            ]);
            return $user;

        });
    }

}

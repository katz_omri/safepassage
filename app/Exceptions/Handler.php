<?php namespace App\Exceptions;

use App;
use Exception;
use Fractal;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;


class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{

		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 * Set the response header status code according to the exception code
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return Response
	 */
	public function render($request, Exception $e)
	{
        if ($e instanceof ModelNotFoundException) {
            $e->setModel($this->getShortName($e->getModel()));
        }

        if ($this->isHttpException($e)) {
            switch ($e->getStatusCode()) {
                // not found
                case 404:
                    return redirect()->route('/');
                    break;
                default:
                    return $this->renderHttpException($e);
                break;
            }
        }
        if ($e instanceof ModelNotFoundException || $e instanceof BaseException) {
            $exception = $this->modifyException($e, $request);
            return response(Fractal::item($exception, $exception->getTransformer(), $exception->toString()));
        }

		return parent::render($request, $e);
	}

    public function modifyException($e, $request)
    {

        return new GeneralException($e, $request);
    }

    public function getShortName($class)
    {
        $refelction = new \ReflectionClass($class);
        return $refelction->getShortName();
    }

}

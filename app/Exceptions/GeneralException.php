<?php namespace App\Exceptions;





use Illuminate\Http\Request;
use Lang;
use ReflectionClass;

class GeneralException extends BaseException {

    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */

    protected $exception;
    protected $request;

    function __construct($exception = null, Request $request)
    {
        $this->request = $request;
        $this->exception = $exception;
        parent::init();

        $message = isset($exception) ? $exception->getMessage() : 'Unknown error was found';
        $code = isset($exception) ? $exception->getCode() : '500';
        $this->setCode($code)->setMessage($message);
    }

    function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getTransformer()
    {
        return parent::getTransformer();
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    public function toString()
    {
        $class = !is_null($this->exception) ? $this->exception : $this;
        $reflect = new ReflectionClass(get_class($class));
        return $reflect->getShortName();
    }
}

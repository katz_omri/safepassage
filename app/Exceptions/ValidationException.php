<?php namespace App\Exceptions;

use App\Transformers\ValidationErrorTransformer;
use Illuminate\Contracts\Support\MessageBag;
use Lang;

class ValidationException extends BaseException {

    protected $input;

    function __construct($errors)
    {
//        parent::init();
//        $this->setInput($errors)->setTransformer($this->getTransformer());
        $this->setMessage($errors);
        $this->code = parent::MISSING_PARAMETER;
    }

    function setMessage($message)
    {
        $this->message = $message;
    }

    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    public function getTransformer()
    {
        return new ValidationErrorTransformer();
    }

    public function setInput($input)
    {
        $this->input = $input;
        return $this;
    }

    public function getInput()
    {
        return $this->input;
    }

}

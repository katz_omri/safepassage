<?php namespace App\Exceptions;



use Lang;

class ServiceForbiddenException extends BaseException {

    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */



    function __construct()
    {
        $this->code = parent::SERVICE_FORBIDDEN_EXCEPTION;
        parent::init();
        $this->setMessage();
    }

    public function setMessage()
    {
        $this->message = Lang::get('responses.ServiceForbidden', ['service' => $this->service, 'action' => $this->action]);
    }


    /**
     * Get the affected Service.
     *
     * @return string
     *
     */
    public function getService()
    {
        return $this->service;
    }


    /**
     * Get the affected Action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

}

<?php namespace App\Exceptions;





use Lang;

class InvalidCredentialsException extends BaseException {

    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */

    function __construct()
    {
        parent::init();
        $this->setCode(parent::UNAUTHORIZED);
        $this->setMessage();
    }

    function setMessage()
    {
        $this->message = Lang::get('responses.CredentialsError');
    }

    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }


}

<?php namespace App\Exceptions;



use Lang;

class NotFoundException extends BaseException {

    /**
     * Name of the affected Eloquent model.
     *
     * @var string
     */



    function __construct()
    {

        parent::init();
        $this->code = parent::NOT_FOUND_EXCEPTION;
        $this->setMessage();

    }

    public function setMessage()
    {
        dd($this->service);
        $this->message = Lang::get('responses.ModelNotFoundException', ['service'=> $this->service]);
    }


    /**
     * Get the affected Service.
     *
     * @return string
     *
     */
    public function getService()
    {
        return $this->service;
    }


    /**
     * Get the affected Action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

}

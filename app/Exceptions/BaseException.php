<?php namespace App\Exceptions;



use App\Transformers\BaseErrorTransformer;
use ReflectionClass;
use Route;
use RuntimeException;

abstract class BaseException extends RuntimeException {

    const NOT_FOUND_EXCEPTION = 404;
    const SERVICE_FORBIDDEN_EXCEPTION = 403;
    const MISSING_PARAMETER = 422;
    const UNAUTHORIZED = 401;
    const BAD_REQUEST = 400;
    protected $routeNames;
    protected $service;
    protected $transformer;
    protected $action;
    protected $request;
    protected $route;


    /**
     *
     */
    function init()
    {
        $this->routeNames = explode('.',Route::currentRouteName());
        $this->buildObject();

    }

    /**
     * if ( explode > 2 )
     *
     */
    private function buildObject()
    {

        // Action will always be in the last position of the array.
//        if (isset($this->routeNames)) {
//            $action = $this->getActionFromPath();
//        }

        $action = array_splice($this->routeNames, count($this->routeNames) - 1);
        $this->setAction($action[0])->setService($this->routeNames);

    }

    private function getActionFromPath()
    {
        dd(Route::getAction());
    }

    public function setTransformer($transformer)
    {
        $this->transformer = new $transformer;
    }

    public function parseService()
    {
        return explode('.', $this->routeNames)[0];
    }/**
    /**
     * Set the affected Action.
     *
     * @param $action
     * @return $this
     *
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }/** @noinspection PhpDocSignatureInspection */

    /**
     * Set the affected Service.
     *
     * @param $service
     * @return $this
     *
     */

    public function setService($service)
    {
        if (is_array($service) && count($service) == 1) {
            $this->service .= $service[0];
            return $this;
        }

        $splicedServiceArray = array_splice($this->routeNames, 0, 1);
        $this->service .= $splicedServiceArray[0] . '>';

        $this->setService($this->routeNames);
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getTransformer()
    {
        return new BaseErrorTransformer();
    }

    abstract function getService();
    abstract function getAction();


    public function toString()
    {
        $reflect = new ReflectionClass(get_class($this));
        return $reflect->getShortName();
    }

}

<?php

namespace App\Exceptions;
use Lang;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


/**
 * Created by PhpStorm.
 * User: omri.katz
 * Date: 10/16/2015
 * Time: 14:52
 */

class TokenException extends BaseException {

    function __construct($message, $code)
    {
        parent::init();
        $this->setCode($code);
        $this->setMessage($message);
    }

    function setMessage($message)
    {
        $this->message = Lang::get('responses.' . $message, ['service'=> $this->service, 'action' => $this->action]);
        if ( is_subclass_of($message, \Exception::class)) {
            $this->message = $message->getMessage();
        }
        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get the affected Eloquent model.
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }
}

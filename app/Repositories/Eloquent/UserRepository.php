<?php namespace App\Repositories\Eloquent;

use Config;
use League\Flysystem\Exception;
use RepositoriesInterface;
use App\Traits\Authorizable;
use DB;

class UserRepository extends Repository {

    use Authorizable;
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\User::class;
    }

    function transformer()
    {
        return \App\Transformers\UserTransformer::class;
    }
    public function get($relations)
    {
        return (method_exists($this->model, $relations)) ? $this->model->$relations() : '';

//        return $this->model->
    }


}
<?php namespace App\Repositories\Eloquent;

use RepositoriesInterface;


class RoleRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Role::class;
    }

    function transformer()
    {
        return \App\Transformers\RoleTransformer::class;
    }




}
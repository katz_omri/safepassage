<?php namespace App\Repositories\Eloquent;

use RepositoriesInterface;


class ArticleRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Article::class;
    }

    function transformer()
    {
        return \App\Transformers\ArticleTransformer::class;
    }




}
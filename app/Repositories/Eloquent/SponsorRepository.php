<?php namespace App\Repositories\Eloquent;


use RepositoriesInterface;
use App\Traits\Authorizable;

class SponsorRepository extends Repository {

    use Authorizable;
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Sponsor::class;
    }

    function transformer()
    {
        return \App\Transformers\SponsorTransformer::class;
    }
    public function get($relations)
    {
        return (method_exists($this->model, $relations)) ? $this->model->$relations() : '';

    }

}
<?php namespace App\Repositories\Eloquent;

use RepositoriesInterface;


class UserLoginDataRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\UserLoginData::class;
    }

    function transformer()
    {
        return \App\Transformers\UserLoginDataTransformer::class;
    }




}
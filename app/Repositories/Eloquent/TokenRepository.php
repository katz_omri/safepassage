<?php namespace App\Repositories\Eloquent;

use RepositoriesInterface;


class ArticleRepository extends Repository {


    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Token::class;
    }

    function transformer()
    {
        return \App\Transformers\TokenTransformer::class;
    }




}
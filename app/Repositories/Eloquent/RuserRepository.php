<?php namespace App\Repositories\Eloquent;


use RepositoriesInterface;
use App\Traits\Authorizable;

class RuserRepository extends Repository {

    use Authorizable;
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return \App\Models\Ruser::class;
    }

    function transformer()
    {
        return \App\Transformers\RuserTransformer::class;
    }
    public function get($relations)
    {
        return (method_exists($this->model, $relations)) ? $this->model->$relations() : '';

    }

}
<?php namespace App\Repositories\Eloquent;


use App\Exceptions\ModelNotFoundException;
use App\Repositories\RepositoryInterface;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Illuminate\Support\Facades\Validator;
use League\Fractal\TransformerAbstract;
use PhpSpec\Exception\Exception;

/**
 * Class Repository
 * @package
 */
abstract class Repository implements RepositoryInterface {

    /**
     * @var App
     */
    private $app;

    /**
     * @var
     */
    protected $model;
    protected $transformer;

    /**
     * @param App $app
     * @throws
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
//        $this->makeTransformer();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();
    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->all($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);

    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws Mod
     */
    public function find($id, $columns = array('*')) {
        $query = $this->model->find($id, $columns);
        if ( ! $query)
        {
            throw new ModelNotFoundException;
        }
        return $query;
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        $query = $this->model->where($attribute, '=', $value)->paginate();
        if ( $query->total() === 0)
        {
//            throw new NotFoundException;
        }
        return $query;
    }

    public function toString()
    {
        return $this->model->toString();
    }



    public function rules()
    {
        $model = $this->app->make($this->model());
        if ( ! $model['rules'])
        {
            throw new Exception;
        }
        return $model['rules'];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws RepositoryException
     */
    public function makeModel() {
        $model = $this->app->make($this->model());
        if (!$model instanceof Model)
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }

    public function validate($data)
    {
        return Validator::make($data, $this->rules());
    }

    public function getTransformer()
    {
        return $this->model->getTransformer();
    }

    public function setTransformer($transformer)
    {
        $this->model->setTransformer($transformer);
        return $this;
    }



}
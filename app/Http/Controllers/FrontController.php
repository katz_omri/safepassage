<?php

namespace App\Http\Controllers;

use App;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Forms\ContactUsMailForm;


class FrontController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front', ['env' => App::environment()]);
    }



    public function sendMail(ContactUsMailForm $form)
    {
        $mail = $form->save(true);
        return $this->item($mail)->respond();

    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Cache;
use Illuminate\Http\Request;

use App\Http\Requests;
use Input;

class CategoryController extends ApiController
{

    protected $pageSize;
    protected $category;

    /**
     * @param Category $category
     */
    function __construct(Category $category)
    {
        parent::__construct();
        $this->category = $category->with('posts');
        $this->pageSize = Input::get('pageSize');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param null $filter
     */
    public function index(Request $request)
    {
        $name = $request->input('name');
        if ($name) {
            return $this->item(
                $this->category->getModel()->findBy('name', $name)
            )->respond();
        }
        return $this->paginate($this->category->paginate())->respond();
    }

    public function showPosts($id)
    {
        $minutes = 5;
        $value = Cache::remember('posts_' . $id, $minutes, function() use ($id) {
            return $this->category->with('posts')->findOrFail($id);
        });
        return $this->collection($value->posts)->respond();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

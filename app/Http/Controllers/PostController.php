<?php namespace App\Http\Controllers;


use App\Exceptions\ServiceForbiddenException;
use Auth;
use Input;
use Cache;
use App\Models\Post;
use App\Http\Requests;
use Illuminate\Http\Request;


/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class PostController extends ApiController {


    /**
     *
     */
    protected $pageSize;
    protected $post;

    /**
     * @param Post $post
     */
    function __construct(Post $post)
    {
        parent::__construct();
        $this->post = $post;
        $this->pageSize = Input::get('pageSize');
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // expire after minutes
        $minutes = 5;
        $groupId = $request->input('groupId');

        $value = Cache::remember('posts', $minutes, function() use ($groupId) {
            if ( ! $groupId) {
                return $this->post->paginate();
            }
            return $this->post->where('group_id','=', $groupId)->paginate();
        });


        return $this->paginate($value)
            ->respond();

    }


    /**
     * Display the specified resource.
     *
     * @todo only the auth user can see their own user info
     * @todo only instructors roles or above can get the user's info
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->item($this->post->find($id))->respond();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $post = $this->post->find($id);
        $post->title = $request->input('title');
        $post->data = $request->input('data');
        $post->save();

        foreach($post->categories()->get() as $category) {
            Cache::forget('posts_' . $category->id);
        }
        Cache::forget('content');
        Cache::forget('posts');
        return $this->item($post)->respond();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function transformer()
    {
        return \App\Transformers\PostTransformer::class;
    }

}

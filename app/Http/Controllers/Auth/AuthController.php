<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Models\Role;
use Auth;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;
use Validator;
use App\Repositories\Eloquent\SponsorRepository as Sponsor;
use App\Models\User as User;
use App\Http\Controllers\Controller;
use App\Traits\Auth\ThrottlesLogins;
use App\Repositories\Eloquent\UserLoginDataRepository as UserLoginData;
use App\Traits\Auth\AuthenticatesAndRegistersUsers as AuthenticatesAndRegistersUsers;


class AuthController extends ApiController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $sponsor;
    protected $userLoginData;
    protected $user;
    protected $redirectPath = '/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @param Sponsor $sponsor
     * @param UserLoginData $userLoginData
     * @param User $user
     */
    public function __construct(Sponsor $sponsor, UserLoginData $userLoginData, User $user)
    {
        $this->user = $user;
        $this->sponsor = $sponsor;
        $this->userLoginData = $userLoginData;
        $this->middleware('auth', ['except' => ['postRegister', 'postLogin']]);
        parent::__construct();
    }



    public function user()
    {
        $user = Cache::remember('user', Carbon::now()->addMinutes(10), function () {
            return Auth::user();
        });
        $function = new \ReflectionClass($user);
        Log::info(__METHOD__ . ': authenticated user is:' . $user);
        return $this->item($user, $function->getShortName())->respond();
    }

    public function role()
    {

        $expiresAt = Carbon::now()->addMinutes(10);
        $value = Cache::remember('role', $expiresAt, function() {
            $role = Auth::user()->role();
            return $this->item($role)->respond();
        });
        return $value;
    }



    public function authenticated($request, $user)
    {
        return $this->item($user)->respond();
    }

    public function getLogout(Request $request)
    {
        Cache::flush();
        Log::info(__METHOD__ . ': User just logged '.$request->user());
        Auth::logout();
        return 'true';
    }
}

<?php namespace App\Http\Controllers;

use App\Contracts\Transformable;
use App\Http\Requests;
use Fractal;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use JsonSerializable;
use League\Fractal\TransformerAbstract;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Container\Container as App;
use Log;


/**
 * Class ApiController
 * @property  input
 * @package App\Http\Controllers
 */
abstract class ApiController extends BaseController {

    use ValidatesRequests, AuthorizesRequests;
	//
    /**
     * @var int
     */

    protected $statusCode = 200;
    /**
     * @var App
     */
    private $app;
    /**
     * @var
     */
    protected $response;
    /**
     * @var
     */
    protected $transformer;


    /**
     * @throws Exception
     */
    function __construct()
    {
        $this->app = new App;
    }

    protected function parseIncludes($include)
    {
        Fractal::parseIncludes($include);
        return $this;
    }



    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @param $data
     * @param null $resourceKey
     * @return mixed
     */
    public function item(Transformable $data, $resourceKey = null)
    {
        $resourceKey = is_null($resourceKey) ? $data->toString() : $resourceKey;
        $this->response = Fractal::item($data, $data->getTransformer(), $resourceKey);
        return $this;
    }


    /**
     * @param $data
     * @param null $resourceKey
     * @return $this
     */
    public function paginate(LengthAwarePaginator $data, $resourceKey = null)
    {
        $resourceKey = is_null($resourceKey) ? $data->first()->toString() : $resourceKey;
        $this->response = Fractal::paginatedCollection($data, $data->first()->getTransformer(), $resourceKey);
        return $this;
    }

    /**
     * @param $data
     * @param null $resourceKey
     * @return $this
     */
    public function collection(JsonSerializable $data, $resourceKey = null)
    {
        $resourceKey = is_null($resourceKey) ? $data->first()->toString() : $resourceKey;
        $this->response = Fractal::collection($data, $data->first()->getTransformer(), $resourceKey);
        return $this;
    }


    /**
     * @return array
     */
    public function respond()
    {
        $response = $this->response;
        Log::info(__METHOD__ . 'A response was just sent  '. response($response));
        return response($response, $this->statusCode);
    }


}

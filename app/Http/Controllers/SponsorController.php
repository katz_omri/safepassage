<?php namespace App\Http\Controllers;


use App\Http\Validators\ValidatorFactory;
use Auth;
use DB;
use Gate;
use Input;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\User as User;
use App\Models\Sponsor as Sponsor;
use App\Exceptions\ValidationException;
use App\Exceptions\ServiceForbiddenException;
use App\Http\Validators\FactoryMethod as ValidatorsFactory;
use App\Http\Validators\SponsorValidator as Validator;


/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class SponsorController extends ApiController {


    /**
     *
     */
    protected $pageSize;
    protected $sponsor;
    protected $user;


    protected $validators = array(
        ValidatorsFactory::USER,
        ValidatorsFactory::SPONSOR
    );

    /**
     * @param Sponsor $sponsor
     * @param User $user
     */
    function __construct(Sponsor $sponsor, User $user)
    {
        parent::__construct();
        $this->sponsor = $sponsor;
        $this->user = $user;
        $this->pageSize = Input::get('pageSize');
    }


    /**
     * Display a listing of the resource.
     *
     * @param null $id
     * @return Response
     */
    public function index($id = null, Request $request)
    {
        if (Gate::denies('sponsor-own', $id)) {
            throw new ServiceForbiddenException();
        }

        if ($id === null) {
            return $this->paginate($this->sponsor->paginate(25))->respond();
        }
        $response = $this->getUsers($id);
        return $this->paginate($response)->respond();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param ValidatorsFactory $validatorsFactory
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ValidatorsFactory $validatorsFactory)
    {
        DB::beginTransaction();

        $sponsor = $request->input('sponsor');
        $user = $request->input('user');
        //create sponsor validator.
        $sponsorValidator = $validatorsFactory->create(ValidatorFactory::SPONSOR, $sponsor);
        $userValidator = $validatorsFactory->create(ValidatorFactory::USER, $user);
//        $loginDataValidator = $validatorsFactory->create(ValidatorFactory::LOGINDATA, $user);
        if ($sponsorValidator->fails()) {
            throw new ValidationException($sponsorValidator->errors());
        }
        $sponsor['type'] = $sponsor['type']['name'];
        $sponsor['country'] = $sponsor['country']['name'];
        $user['country'] = $user['country']['name'];
        // validation is fine, create the sponsor
        $sponsor = $this->sponsor->create($sponsor);

        // Validate the user now, must do that to make sure no duplicate users
        if ($userValidator->fails()) {
            throw new ValidationException($userValidator->errors());
        }
        $user['password'] = bcrypt($user['password']);

        $user = $sponsor->addUser($user);

        // Validate the login data information to prevent duplicate.
        // This information will be used to login the user.
//        if ($loginDataValidator->fails()) {
//            throw new ValidationException($loginDataValidator->errors());
//        }


//        $user->enableLogin($user);


        // need to assign the user a role.

        $user->assignRole('sponsor');


        DB::commit();

        // Log the user in after successful creation

        Auth::login($user);

        // return the registered user.
        return $this->item($user)->respond();

    }


    /**
     * Display the specified resource.
     *
     * @todo only the auth user can see their own user info
     * @todo only instructors roles or above can get the user's info
     * @param $sponsor_id
     * @param null $user_id
     * @param Request $request
     * @return Response
     * @internal param int $id
     */
    public function show($sponsor_id, $user_id = null, Request $request)
    {
        $user = $this->sponsor->find($sponsor_id)->users()->find($user_id);
        return $this->item($this->user->find($user_id))->respond();
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    function transformer()
    {
        return \App\Transformers\SponsorTransformer::class;
    }

    protected function getUsers($id)
    {
        return $id ? Sponsor::findOrFail($id)->users()->paginate() : User::paginate(15);
    }

}

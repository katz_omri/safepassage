<?php namespace App\Http\Controllers;


use App\Exceptions\ServiceForbiddenException;
use App\Http\Requests;
use App\Models\Role;
use App\Models\Sponsor;
use App\Models\User as User;

use Auth;
use Gate;
use Illuminate\Contracts\Auth\Guard;
use Input;
use League\Fractal\Manager;


/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UserController extends ApiController {


    /**
     *
     */
    protected $pageSize;
    protected $user;
    protected $sponsor;

    /**
     * @param Guard $auth
     * @param User $user
     */
    function __construct(Guard $auth, User $user, Sponsor $sponsor)
    {
        parent::__construct();
        $this->user = $user;
        $this->sponsor = $sponsor;
        $this->auth = $auth;
        $this->pageSize = Input::get('pageSize');
    }


    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id = null)
	{
        if (Gate::denies('sponsor-own', $id)) {
            throw new ServiceForbiddenException();
        }
        if ($id == null) {
            return $this->paginate($this->user->paginate())->respond();
        }
        $response = $this->sponsor->find($id)->users()->paginate();
        return $this->paginate($response)->respond();
    }

    public function role($userId)
    {

        if (!Auth::check()) {
            $role = new Role([
                'name' => 'anonymous',
                'display_name' => 'anonymous role'
            ]);
            return $this->item($role, $role->toString())->respond();
        }
        $role = Auth::user()->role()->first();
        if (Gate::denies('show',array( $role, $userId))) {
            throw new ServiceForbiddenException;
        }

        return $this->item(User::findOrFail($userId)->roles()->firstOrFail())->respond();

        return $this->item($role, $role->toString())->respond();


    }

	/**
	 * Display the specified resource.
     *
	 * @todo only the auth user can see their own user info
     * @todo only instructors roles or above can get the user's info
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $sponsor)
    {
        dd($sponsor);
        $authUser = $this->auth->user();
        return $this->item($this->user->findOrFail($id))->respond();

    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    function transformer()
    {
        return \App\Transformers\UserTransformer::class;
    }



}

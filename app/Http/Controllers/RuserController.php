<?php

namespace App\Http\Controllers;

use Gate;
use Input;
use App\Models\Ruser;
use App\Http\Requests;
use App\Models\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;


class RuserController extends ApiController {

    protected $ruser;
    protected $sponsor;
    protected $auth;
    protected $pageSize;

    function __construct(Ruser $ruser, Sponsor $sponsor, Guard $auth)
    {
        parent::__construct();
        $this->ruser = $ruser;
        $this->sponsor = $sponsor;
        $this->auth = $auth;
        $this->pageSize = Input::get('pageSize');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if (Gate::denies('sponsor-own', $id)) {
            throw new ServiceForbiddenException();
        }
        if ($id == null) {
            return $this->paginate($this->ruser->paginate())->respond();
        }
        $response = $this->sponsor->find($id)->rusers()->paginate();
        return $this->paginate($response)->respond();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php namespace App\Http\Middleware;

use App\Exceptions\ServiceForbiddenException;
use App\Transformers\ErrorTransformer;
use Closure;

use Illuminate\Contracts\Auth\Guard;
use Route;


class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
            throw (new ServiceForbiddenException(Route::currentRouteName()));
		}
		return $next($request);
	}

}

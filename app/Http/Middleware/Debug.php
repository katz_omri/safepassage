<?php namespace App\Http\Middleware;

use Closure;

class Debug {


    protected $except = [
        'mail/*',
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->exists('Debug')) {
            config(['app.debug' => true]);
        }
        return $next($request);
    }

}

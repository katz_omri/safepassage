<?php

namespace App\Http\Middleware;

use App\Exceptions\TokenException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class GetUserFromToken extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws TokenException
     */
    public function handle($request, \Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken()) {
            $this->events->fire('tymon.jwt.absent');
            throw new TokenException('token_not_provided', '400');
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            // Token Expired.
            $this->events->fire('tymon.jwt.expired');
            throw new TokenException('token_expired', $e->getStatusCode());
        } catch (JWTException $e) {
            // Invalid Token.
            $this->events->fire('tymon.jwt.invalid');
            throw new TokenException($e, $e->getStatusCode());
        }

        if (! $user) {
            $this->events->fire('tymon.jwt.user_not_found');
            throw new TokenException('user_not_found', '404');
        }
        // Everything is okay, continue.
        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }
}

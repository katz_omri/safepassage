<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;


class CacheMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = $next($request);
        $response->header('Cache-Control', 'max-age=120');
        return $response;
    }

}
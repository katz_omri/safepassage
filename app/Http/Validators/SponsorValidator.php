<?php namespace App\Http\Validators;
use App\Contracts\ValidatorContract;
use Illuminate\Http\Request;
use Validator;

/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:13
 */

class SponsorValidator implements ValidatorContract
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $validator;


    protected $request;


    public static function rules()
    {
        return [
            'email'               => 'required|unique:sponsor|max:255|email',
            'type'                => 'required',
            'name'                => 'required|max:255',
            'phone'               => 'required|max:255',
            'address'             => 'required|max:255',
            'sponsorship_quota'   => 'optional|max:255',
            'number_of_members'   => 'optional',
            'representative_name' => 'required|max:255',
            'previous_experience' => 'optional'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'A sponsor email is required',
            'email.unique' => 'The sponsor email has already been taken',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param array $request
     * @return \Illuminate\Validation\Validator
     */

    public function make()
    {
        return Validator::make($this->request, self::rules(), $this->messages());
    }


    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }



}
<?php namespace App\Http\Validators;
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:41
 */


/**
 * class FactoryMethod
 */
abstract class FactoryMethod
{


    const USER = 1;
    const SPONSOR = 2;
    const LOGIN = 3;
    const LOGINDATA = 4;
    const MAIL = 5;
    /**
     * The children of the class must implement this method
     *
     * Sometimes this method can be public to get "raw" object
     *
     * @param string $type a generic type
     *
     * @return VehicleInterface a new vehicle
     */
    abstract protected function createValidator($type);

    /**
     * Creates a new validator
     *
     * @param int $type
     *
     * @param $request
     * @return ValidatorInterface for a new model
     */
    public function create($type, $request)
    {
        return $this->createValidator($type)->setRequest($request)->make();
    }
}
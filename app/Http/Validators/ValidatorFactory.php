<?php namespace App\Http\Validators;
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/5/15
 * Time: 12:19
 */

    class ValidatorFactory extends FactoryMethod {

        /**
         * The children of the class must implement this method
         *
         * Sometimes this method can be public to get "raw" object
         *
         * @param string $type a generic type
         *
         * @return VehicleInterface a new vehicle
         */
        protected function createValidator($type)
        {
            switch ($type) {
                case parent::USER:
                    return new UserValidator();
                    break;
                case parent::SPONSOR:
                    return new SponsorValidator();
                    break;
                case parent::LOGIN:
                    return new LoginValidator();
                    break;
                case parent::LOGINDATA:
                    return new LoginDataValidator();
                case parent::MAIL:
                    return new MailValidator();
                default:
                    throw new \InvalidArgumentException("$type is not a valid vehicle");
            }
        }

    }
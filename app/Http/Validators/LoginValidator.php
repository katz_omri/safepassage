<?php namespace App\Http\Validators;

use Validator;
use App\Contracts\ValidatorContract;

/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:13
 */

class LoginValidator implements ValidatorContract
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $validator;


    protected $request;


    public static function rules()
    {
        return [
            'password'    => 'required|min:6',
            'login_email' => 'required|email|max:255',
        ];
    }
    public function messages()
    {
        return [
            'email.required'    => 'A login email is required',
            'password.required' => 'A login password is required'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param array $request
     * @return \Illuminate\Validation\Validator
     */

    public function make()
    {
        return Validator::make($this->request, self::rules(), $this->messages());
    }


    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }



}
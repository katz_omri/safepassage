<?php namespace App\Http\Validators;

use Validator;
use App\Contracts\ValidatorContract;

/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:13
 */

class LoginDataValidator implements ValidatorContract
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $validator;


    protected $request;


    public static function rules()
    {
        return [
            'password'    => 'required|max:255',
            'last_name'   => 'required|max:255',
            'first_name'  => 'required',
            'login_email' => 'required|unique:user_login_data|max:255|email'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'A login email email is required',
            'email.unique' => 'The login email has already been taken',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     * @return \Illuminate\Validation\Validator
     * @internal param array $request
     */

    public function make()
    {
        return Validator::make($this->request, self::rules(), $this->messages());
    }


    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }



}
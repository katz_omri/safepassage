<?php namespace App\Http\Validators;

use App\Contracts\ValidatorContract;
use Illuminate\Http\Request;
use Validator;

/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:13
 */

class UserValidator implements ValidatorContract
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $rules;

    protected $request;

    public static function rules()
    {
        return [
            'email'      => 'required|unique:user|max:255|email',
            'phone'      => 'optional|max:255',
            'address'    => 'required|max:255',
            'last_name'  => 'required|max:255',
            'first_name' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'A user email is required',
            'email.unique' => 'The user email has already been taken',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return \Illuminate\Validation\Validator
     */

    public function make()
    {
        return Validator::make($this->request, self::rules(), $this->messages());
    }


    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }

}
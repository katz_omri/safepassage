<?php namespace App\Http\Validators;
use App\Contracts\ValidatorContract;
use Illuminate\Http\Request;
use Validator;

/**
 * Created by PhpStorm.
 * User: omri
 * Date: 12/4/15
 * Time: 16:13
 */

class MailValidator implements ValidatorContract
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $validator;


    protected $request;

    /*
     *         $email = $request->input('email');
        $name = $request->input('name');
        $messageContent = $request->input('message');
     */
    public static function rules()
    {
        return [
            'email'=> 'required|email',
            'name' => 'required',
            'message' => 'required',
            'recaptcha' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Email is required',
            'name.required' => 'Name is required',
            'message.required' => 'Message body is required',
            'recaptcha.required' => 'Recaptcha is required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param array $request
     * @return \Illuminate\Validation\Validator
     */

    public function make()
    {
        return Validator::make($this->request, self::rules(), $this->messages());
    }


    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }



}
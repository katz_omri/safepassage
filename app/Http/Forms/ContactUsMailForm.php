<?php

namespace App\Http\Forms;

use App\Models\Mail;
use App\Jobs\SendContactUsMail;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ContactUsMailForm extends Form {

    use DispatchesJobs;

    protected function rules()
    {
        return [
            'body'      => 'required',
            'name'      => 'required',
            'email'     => 'required|email',
            'subject'   => 'required',
            'recaptcha' => 'required'
        ];
    }

    public function persist()
    {
        $mail = new Mail($this->request->toArray());
        $mail->status = Mail::NOT_STARTED;
        $mail->type = Mail::CONTACT_US;
        $mail->save();
        $this->dispatch(new SendContactUsMail($mail));
        return $mail;
    }

    protected function messages()
    {
        return [
            'recaptcha.required' => 'Please let us know you are not a robot',
        ];
    }
}
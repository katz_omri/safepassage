<?php namespace App\Http\Forms;

use App\Exceptions\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;

/**
 * Class Form
 * @package App\Http\Forms
 */
abstract class Form {

    use ValidatesRequests;

    /**
     * @var Request
     */

    protected $request;
    /**
     * @var
     */

    protected $errors;

    /**
     * @var \Illuminate\Validation\Validator
     */

    protected $validator;
    /**
     * @var
     */
    protected $rules;
    /**
     * @var
     */
    protected $messages;

    /**
     * @param Request $request
     */
    function __construct(Request $request = null)
    {
        $this->request = $request ?: request();
        $this->validator = Validator::make($this->request->toArray(), $this->rules(), $this->messages());
    }


    /**
     * @return mixed
     */
    abstract protected function rules();

    /**
     * @return mixed
     */
    abstract protected function messages();


    /**
     * @param bool $withCaptcha
     * @return bool
     */
    protected function isValid($withCaptcha = false)
    {
        if ($withCaptcha) {
            $secret = env('RECAPTCHA_SECRET_KEY');
            $recaptcha = new \ReCaptcha\ReCaptcha($secret);
            $resp = $recaptcha->verify($this->request->input('recaptcha'));
            if ( ! $resp->isSuccess()) {
                $this->validator->errors()->add('recaptcha', 'recaptcha validation failed');
                return false;
            }
            $this->captchaValid = true;
        }
        return !$this->validator->errors()->messages();

    }

    /**
     * @param bool $withCaptcha
     * @return mixed
     */
    public function save($withCaptcha = false)
    {
        if (!  $this->isValid($withCaptcha)) {
            throw new ValidationException($this->validator->errors());
        }
        return $this->persist();

    }
}

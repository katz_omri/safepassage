<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => 'api/v1'], function () {

    Route::group(['middleware' => ['throttle:3,1']], function () {
        Route::post('mail/send', array('as' => 'mail.send', 'uses' => 'FrontController@sendMail'));
    });
    // Authentication routes...
    Route::post('auth/login', array('as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin'));
    Route::get('auth/logout', array('as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout'));
    // Registration routes...
    Route::get('auth/register', array('as' => 'auth.register', 'uses' => 'Auth\AuthController@getRegister'));
    Route::post('auth/register', array('as' => 'auth.register', 'uses' => 'Auth\AuthController@postRegister'));

    Route::post('sponsors', array('as' => 'sponsors.store', 'uses' => 'SponsorController@store'));

    Route::group(['middleware' => ['admin']], function () {
        Route::get('sponsors', array('as' => 'sponsors.index', 'uses' => 'SponsorController@index'));
    });


    Route::get('auth/user', array('as' => 'auth.user', 'uses' => 'Auth\AuthController@user'));
    Route::get('auth/role', array('as' => 'auth.role', 'uses' => 'Auth\AuthController@role'));
    Route::get('auth/anonymous', array('as' => 'auth.anonymous', 'uses' => 'Auth\AuthController@getAnonymous'));
    Route::group(['middleware' => ['auth']], function () {
        Route::get('sponsors/{id}/users', array('as' => 'user.index', 'uses' => 'UserController@index'));
        Route::get('sponsors/{id}/rusers', array('as' => 'ruser.index', 'uses' => 'RuserController@index'));
        Route::get('users', array('as' => 'users.index', 'uses' => 'UserController@index'));
        Route::get('users/{id}', array('as' => 'users.show', 'uses' => 'UserController@show'));
        Route::get('users/{id}/sponsor', array('as' => 'users.sponsor.show', 'uses' => 'UserSponsorController@show'));

    });
    Route::get('posts', array('as' => 'posts.index', 'uses' => 'PostController@index'));
    Route::get('posts/{id}', array('as' => 'posts.show', 'uses' => 'PostController@show'));
    Route::post('posts/{id}', array(
        'as'         => 'posts.update',
        'uses'       => 'PostController@update',
        'middleware' => 'admin'
    ));

    Route::get('categories', array('as' => 'categories.index', 'uses' => 'CategoryController@index'));
    Route::get('categories/{id}', array('as' => 'categories.show', 'uses' => 'CategoryController@show'));
    Route::get('categories/{id}/posts', array('as' => 'categories.posts', 'uses' => 'CategoryController@showPosts'));

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');

});

Route::group(['middleware' => ['cache']], function() {

    Route::get('/', array('as' => '/', 'uses' => 'FrontController@index'));

});

//Route::any('{path?}',array('as' => '/', 'uses' => 'FrontController@index'))->where("path", ".+");
<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Validator;

class StoreSponsorRequest extends Request
{


    /**
     * Redirect route when errors occur.
     *
     * @var string
     */
    protected $redirectAction = 'ErrorController@';
    protected $errorBag = 'errors';


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:sponsor|max:255',
            'organization_name' => 'required|max:255',
            'type' => 'required',
            'representative_name' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|max:255',
            'sponsorship_quota' => 'required|max:255',
            'previous_experience' => 'optional',
            'number_of_members' => 'optional'
        ];
    }

}

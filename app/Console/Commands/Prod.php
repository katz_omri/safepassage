<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Flysystem\Exception;
use Symfony\Component\Process\Process;


class Prod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:deploy {env=dev} {--debug}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $process;
    protected $env;
    protected $currentEnv;
    protected $currentDebugMode;
    protected $debug;

    protected static $_envMapping = [
        'prod' => 'production',
        'dev'  => 'local'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->currentEnv = env('APP_ENV');
        $this->currentDebugMode = env('APP_DEBUG') ? 'true' : 'false';
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->env = $this->argument('env') ?: 'dev';
        $this->debug = $this->option('debug') ? 'true': 'false';
        $this->process = new Process('gulp ' . $this->env);
        $this->process->setTimeout(1000000);
        $this->setEnvInEnvironmentFile(self::$_envMapping[$this->env]);
        if ($this->env == 'dev' && !$this->option('debug')) {
            $this->debug = 'true';
        }
        $this->setDebugInEnvironmentFile($this->debug);

        $this->process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > '.$buffer;
            } else {
                $this->info($buffer);
            }
        });

    }


    /**
     * Set the application env in the environment file.
     *
     * @param  string  $key
     * @return void
     */
    protected function setEnvInEnvironmentFile($key)
    {

        file_put_contents($this->laravel->environmentFilePath(), str_replace(
            'APP_ENV='.$this->currentEnv,
            'APP_ENV='.$key,
            file_get_contents($this->laravel->environmentFilePath())
        ));
    }

    /**
     * Set the application debug mode in the environment file.
     *
     * @param  string  $key
     * @return void
     */
    protected function setDebugInEnvironmentFile($debug)
    {

        file_put_contents($this->laravel->environmentFilePath(), str_replace(
            'APP_DEBUG='.$this->currentDebugMode,
            'APP_DEBUG='.$debug,
            file_get_contents($this->laravel->environmentFilePath())
        ));
    }

}

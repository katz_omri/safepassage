<?php

namespace App\Models;


use Mail as Mailer;
use App\Enums\MailType;
use App\Enums\MailStatus;
use App\Contracts\Transformable;

class Mail extends BaseModel implements Transformable, MailType, MailStatus
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mail';

    protected $mailer;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'email',
        'user_id',
        'type',
        'subject',
        'status',
        'name',
    ];



    function transformer()
    {
        return \App\Transformers\MailTransformer::class;
    }

    function send()
    {
        Mailer::send('emails.newEmail', [
            'email' => $this->email,
            'name' => $this->name,
            'subject' => $this->subject,
            'body' => $this->body
        ], function ($m) {
            $m->from('self@safepassage.ca', 'Safe Passage Canada inquires: ' . $this->name);
            foreach ($this->recipients as $recipient) {
                $m->to($recipient->email, 'New inquiry from: ' . $this->name)->subject($this->subject);
            }
            $this->status = Mail::SENT;
            $this->save();
        });
    }


    public function recipients()
    {
        return $this->hasMany(\App\Models\Recipient::class);
    }

    public function setRecipients($recipients)
    {
        $recipients = is_array($recipients) ? $recipients : func_get_args();
        foreach($recipients as $recipient) {
            $recipient = new Recipient(['email' => $recipient]);
            $this->recipients()->save($recipient);
        }
        return $this;
    }

}

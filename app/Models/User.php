<?php namespace App\Models;

use App\Contracts\Transformable;
use App\Contracts\UserContract;
use App\Exceptions\GeneralException;
use Illuminate\Http\Request;
use App\Traits\HasRoleTrait;
use Config;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


/**
 * Class User
 * @package App\Models
 */
class User extends BaseUserModel implements AuthenticatableContract,
                                        CanResetPasswordContract,
                                        AuthorizableContract,
                                        Transformable,
                                        UserContract
{

    use Authenticatable, Authorizable, CanResetPassword, HasRoleTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'first_name',
        'last_name',
        'login_email',
        'address',
        'city',
        'country',
        'phone',
        'password',
        'remember_token'
    ];



    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['remember_token', 'password'];


    /**
     * Get the user login data instance
     */
    public function loginData()
    {
        return $this->morphMany('App\Models\UserLoginData', 'authenticatable');
    }



    function transformer()
    {
        return \App\Transformers\UserTransformer::class;
    }


    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function userType()
    {
        return 'PUSER';
    }

    public function getRoles()
    {
        return $this->belongsToMany(Role::class)->orderBy('id', 'desc');
    }

    public function enableLogin($request)
    {
        if ( ! is_array($request)) {
            $this->loginData()->save($request);
            return $this;
        }
        $loginData = UserLoginData::create($request);

        $loginData->login_email = isset($request['login_email']) ? $request['login_email'] : $this->email;
        $loginData->password = bcrypt($request['password']);

        $this->loginData()->save($loginData);
        return $this;
    }


    /**
     * @return mixed
     */
    function disableLogin()
    {

    }



}

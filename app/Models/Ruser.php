<?php namespace App\Models;

use App\Contracts\Transformable;
use App\Contracts\UserContract;
use Config;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * @package App\Models
 */
class Ruser extends BaseUserModel implements AuthenticatableContract,
    AuthorizableContract,
    UserContract,
    Transformable
{

    use Authenticatable, Authorizable;
//    use EntrustUserTrait; // add this trait to your user model

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ruser';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name','last_name', 'email'];


    function transformer()
    {
        return \App\Transformers\RuserTransformer::class;
    }

    public function userType()
    {
        return 'RUSER';
    }

    public function getRoles()
    {
        return Role::where('name', 'refugee')->get()->sortByDesc('id');
    }

    public function isAdmin()
    {
        return false;
    }

    /**
     * Get the user login data instance
     */
    public function loginData()
    {
        return $this->morphMany('App\Models\UserLoginData', 'authenticatable');
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function enableLogin($request)
    {
        if ( ! is_array($request)) {
            $this->loginData()->save($request);
            return $this;
        }
        $loginData = UserLoginData::create($request);

        $loginData->login_email = isset($request['login_email']) ? $request['login_email'] : $this->email;
        $loginData->password = bcrypt($request['password']);

        $this->loginData()->save($loginData);
        return $this;
    }

    /**
     * @return mixed
     */
    function disableLogin()
    {
        // TODO: Implement disableLogin() method.
    }
}

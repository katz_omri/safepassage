<?php namespace App\Models;
use App\Contracts\Transformable;


/**
 * Class User
 * @package App\Models
 */
class Post extends BaseModel implements Transformable
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'user_id', 'data'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    function transformer()
    {
        return \App\Transformers\PostTransformer::class;
    }

    /**
     * Assign the category role to the post.
     *
     * @param $categories
     * @return mixed
     */
    public function assignCategory($categories)
    {
        if ($categories instanceof BaseModel) {
            $this->categories()->save($categories);
            return $this;
        }
        $categories = is_array($categories) ? $categories : func_get_args();
        foreach ($categories as $category) {
            $this->categories()->save(
                Category::whereName($category)->firstOrFail()
            );
        }
        return $this;
    }

    function categories()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'category_post');
    }

}

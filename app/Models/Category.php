<?php

namespace App\Models;


use App\Contracts\Transformable;

class Category extends BaseModel implements Transformable
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    function transformer()
    {
        return \App\Transformers\CategoryTransformer::class;
    }

    function posts()
    {
        return $this->belongsToMany(\App\Models\Post::class, 'category_post');
    }

//    protected $transformer = \App\Transformers\ArticleTransformer::class;

}

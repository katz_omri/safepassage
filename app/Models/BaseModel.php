<?php namespace App\Models;

use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;
use ReflectionClass;
use Illuminate\Container\Container as App;

/**
 * Class Category
 * @package App\Models
 */
abstract class BaseModel extends Model {


    protected $app;

    abstract function transformer();
    protected $transformer;

    function __construct(array $attributes = [])
    {

        $this->app = new App;
        $this->setTransformer($this->transformer());
        parent::__construct($attributes);

    }
    public function setTransformer($transformer = null)
    {
        $this->transformer = ($transformer instanceof TransformerAbstract) ? $transformer : $this->makeTransformer($transformer);
        return $this;
    }

    public function getTransformer()
    {
        return $this->transformer;
    }

    public function makeTransformer($transformer = null)
    {
        $transformer = !is_null($transformer) ? $transformer : $this->transformer();
        return $this->app->make($transformer);
    }



    public function toString()
    {
        $reflect = new ReflectionClass(get_class($this));
        return $reflect->getShortName();
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')) {
        return $this->where($attribute, '=', $value)->first($columns);
    }

}

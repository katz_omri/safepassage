<?php namespace App\Models;
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 11/28/15
 * Time: 18:12
 */

abstract class BaseUserModel extends baseModel {


    abstract function userType();


    public function isActive()
    {
        return $this->loginData()->first()->active;
    }

    public function sponsor()
    {
        return $this->belongsTo(\App\Models\Sponsor::class);
    }



}
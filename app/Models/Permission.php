<?php namespace App\Models;



/**
 * Class Permission
 * @package App\Models
 */

class Permission extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission';


    /**
     * A permission can be applied to roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    function transformer()
    {

        return \App\Transformers\PermissionTransformer::class;
    }
}

<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


/**
 * Class User
 * @package App\Models
 */
class UserLoginData extends BaseModel implements AuthenticatableContract
                                                ,CanResetPasswordContract

{

    use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_login_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login_email',
        'password',
        'first_name',
        'last_name',
        'custom_data'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];


    public function rusers()
    {
        return $this->authenticatable();
    }


    function transformer()
    {
        return \App\Transformers\UserLoginDataTransformer::class;
    }

    public function isAdmin()
    {
        return $this->authenticatable->isAdmin();
    }

    public function role()
    {
        return $this->authenticatable->getRoles()->first();
    }

    public function authenticatable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->authenticatable();
    }

    public function getSponsor()
    {
        return $this->users->sponsor()->first();
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->login_email;
    }


}

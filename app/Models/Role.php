<?php namespace App\Models;

use App\Contracts\Transformable;
use App\Traits\RoleTrait;
use Illuminate\Database\Eloquent\Collection;
use ReflectionClass;

/**
 * Class Role
 * @package App\Models
 */
class Role extends BaseModel implements Transformable {

    const ANONYMOUS = 4;
    const VIEWER = 3;
    const REFUGEE = 2;
    const SPONSOR = 1;
    const ADMIN = 0;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role';

    protected $fillable = ['name', 'display_name'];

    public function toString()
    {
        $reflect = new ReflectionClass(get_class($this));
        return $reflect->getShortName();
    }

    function transformer()
    {
        return \App\Transformers\RoleTransformer::class;
    }

    /**
     * A role may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    public function scopeStrongest($query)
    {
        return $query->orderBy('id', 'desc')->first();
    }


    /*
     * @param  Permission $permission
     * @return mixed
     */
    public function givePermissionTo($permission)
    {
        if (is_string($permission)) {
            return $this->permissions()->save(
                Permission::whereName($permission)->firstOrfail()
            );
        }

        return $this->permissions()->save($permission);
    }

    public function givePermissionsTo($permissions)
    {
        foreach ($permissions as $permission) {
                $this->givePermissionTo($permission);
        }
        return $this;
    }

    public function mapRoles()
    {
        dd($this->name);
    }

}

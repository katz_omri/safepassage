<?php namespace App\Models;

use App\Contracts\Transformable;
use App\Transformers\TokenTransformer;
use League\Fractal\Resource\Collection;
use ReflectionClass;

/**
 * Class Exception
 * @package App\Models
 */
class Token extends BaseModel implements Transformable {

    /**
     * @var null
     */
    public $token;
    protected $id;
    protected $fillable = ['token'];

    function __construct($token = null)
    {
        $array = array('token' => $token, 'id' => rand(0, 1500));
        parent::__construct($array);
        $this->setToken($token);
    }

    /**
     * @param $code
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }


    function transformer()
    {
        return \App\Transformers\TokenTransformer::class;
    }

    /**
     * @return null
     */
    public function getToken()
    {

        return $this->token;
    }



    public function toString()
    {
        $reflect = new ReflectionClass(get_class($this));
        return $reflect->getShortName();
    }

    public function getTransformer()
    {
        return $this->transformer;
    }


}

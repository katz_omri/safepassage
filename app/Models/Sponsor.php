<?php namespace App\Models;
use App\Contracts\Transformable;
use App\Exceptions\ValidationException;
use App\Http\Validators\UserValidator;
use DB;


/**
 * Class User
 * @package App\Models
 */
class Sponsor extends BaseModel implements Transformable {



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sponsor';

    protected static $types = [
        'Sponsorship Agreement Holder',
        'Community sponsor',
        'Group of five',
        'Group of two',
        'Non-profit legal person'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'city',
        'name',
        'type',
        'phone',
        'address',
        'country',
        'login_data_id',
        'number_of_members',
        'organization_name',
        'sponsorship_quota',
        'representative_name',
        'previous_experience',
    ];

    protected $validator;
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->validator = new UserValidator();
    }

    public static function getTypes()
    {
        return self::$types;
    }

    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'sponsor_id');
    }
    public function rusers()
    {
        return $this->hasMany(\App\Models\Ruser::class, 'sponsor_id');
    }

    public function addUser($request)
    {

        if ( ! is_array($request)) {
            return $this->users()->save($request);
        }

        return $this->users()->save(User::create($request));
    }

    public function addRuser($request)
    {
        if ( ! is_array($request)) {
            return $this->rusers()->save($request);
        }
        return $this->rusers()->save(Ruser::create($request));
    }




    public function transformer()
    {
        return \App\Transformers\SponsorTransformer::class;
    }

    public function availableTypes($column)
    {
        $type = DB::select( DB::raw("SHOW COLUMNS FROM $this->table WHERE Field = '$column'") )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }


}

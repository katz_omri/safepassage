<?php namespace App\Models;

use League\Fractal\Resource\Collection;

/**
 * Class Exception
 * @package App\Models
 */
class Exception extends Collection {

    /**
     * @var null
     */
    private $code;
    /**
     * @var null
     */
    private $message;
    /**
     * @var null
     */
    private $input;

    protected $exception;

    /**
     * @param null $code
     * @param null $message
     * @param null $input
     */
    function __construct($code = null, $message = null, $input = null)
    {
//        $this->exception = $exception;
//        $this->code = $exception->getCode();
//        $this->message = $exception->getMessage();
//        dd($this->exception);
        $this->input = $input;
    }


    /**
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param $input
     * @return $this
     */
    public function setInput($input)
    {
        $this->input = $input;
        return $this;
    }

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return null
     */
    public function getInput()
    {
        return $this->input;
    }

}

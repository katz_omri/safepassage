<?php namespace App\Transformers;


use League\Fractal;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeTransformer
 * @package App\Transformers
 */
class ValidationErrorTransformer extends TransformerAbstract {
    /**
     * @param $e
     * @return array
     * @internal param $exceptions
     * @internal param Collection $error
     * @internal param Employee $employee
     */
    public function transform($e)
    {

        return [
            'id'      => $e->getCode(),
            'code'    => $e->getCode(),
            'action'  => $e->getAction(),
            'service' => $e->getService(),
            'message' => $e->getMessage()
        ];
    }

}
<?php namespace App\Transformers;



use League\Fractal;


class UserLoginDataTransformer extends Fractal\TransformerAbstract {



    public function transform($userLoginData)
    {
        return [
            'id'    => $userLoginData->id,
            'email' => $userLoginData->login_email,
        ];
    }


}
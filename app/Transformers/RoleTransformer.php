<?php namespace App\Transformers;

use App\Models\Role;
use League\Fractal;

/**
 *
 * @package App\Transformers
 */
class RoleTransformer extends Fractal\TransformerAbstract {
    /**
     * @param Role $role
     * @return array
     */
    public function transform($role)
    {
        return [
            'id'           => (int) $role->id,
            'name'         => $role->name,
            'description'  => $role->description,
            'display_name' => $role->display_name
        ];
    }
}
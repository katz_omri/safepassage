<?php namespace App\Transformers;


use App\Models\Mail;
use League\Fractal\TransformerAbstract;

/**
 * Class MailTransformer
 * @package App\Transformers
 */
class MailTransformer extends TransformerAbstract {

    /**
     * @param Mail $mail
     * @return array
     */

    public function transform(Mail $mail)
    {
        return [
            'id'       => $mail->id,
            'type'     => $mail->type,
            'email'    => $mail->email,
            'name'     => $mail->name,
            'status'   => $mail->status,
            'body'     => $mail->body,
            'subject'  => $mail->subject,
        ];
    }

}
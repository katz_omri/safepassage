<?php namespace App\Transformers;


use League\Fractal;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeTransformer
 * @package App\Transformers
 */
class TokenTransformer extends TransformerAbstract {
    /**
     * @param $e
     * @return array
     * @internal param $exceptions
     * @internal param Collection $error
     * @internal param Employee $employee
     */
    public function transform($token)
    {
        return [
            'id' => rand(0, 10),
            'token'  => $token->getToken()
        ];
    }

}
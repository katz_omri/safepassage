<?php namespace App\Transformers;


use League\Fractal;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployeeTransformer
 * @package App\Transformers
 */
class NotFoundExceptionTransformer extends TransformerAbstract {
    /**
     * @param $e
     * @return array
     * @internal param $exceptions
     * @internal param Collection $error
     * @internal param Employee $employee
     */

    public function transform($e)
    {
        return [
            'id'      => $e->getCode(),
            'service' => $e->getService(),
            'action'  => $e->getAction(),
            'code'    => $e->getCode(),
            'message' => $e->getMessage(),
        ];
    }

}
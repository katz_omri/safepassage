<?php namespace App\Transformers;


use App\Models\Ruser;

use League\Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class SponsorTransformer extends Fractal\TransformerAbstract {


    public function transform($sponsor)
    {
        return [
            'id'                     => (int) $sponsor->id,
            'name'                   => $sponsor->name,
            'city'                   => $sponsor->city,
            'email'                  => $sponsor->email,
            'address'                => $sponsor->address,
            'sponsorship_quota'      => $sponsor->sponsorship_quota,
            'number_of_members'      => $sponsor->number_of_members,
            'representative_name'    => $sponsor->representative_name,
        ];
    }
}


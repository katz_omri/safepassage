<?php namespace App\Transformers;
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 2/19/16
 * Time: 13:42
 */

use League\Fractal;
use App\Models\Category;


/**
 * @package App\Transformers
 */
class CategoryTransformer extends Fractal\TransformerAbstract {
    /**
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id'      => (int) $category->id,
            'name'   => $category->name,
            'display_name' => $category->display_name
        ];
    }
}
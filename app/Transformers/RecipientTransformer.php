<?php namespace App\Transformers;


use App\Models\Recipient;
use League\Fractal;

class RecipientTransformer extends Fractal\TransformerAbstract {




    public function transform(Recipient $recipient)
    {

        return [
            'id'         => (int) $recipient->id,
            'email'      => $recipient->email,
            'mail_id' => $recipient->mail_id
        ];
    }

}
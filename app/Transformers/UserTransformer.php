<?php namespace App\Transformers;


use App\Contracts\Transformable;
use App\Models\User;

use League\Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {


    protected $availableIncludes = [
        'role'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'role'
    ];

    public function transform($user)
    {
        return [
            'id'         => (int) $user->id,
            'last_name'  => $user->last_name,
            'first_name' => $user->first_name,
            'login_email'=> $user->login_email,
        ];
    }

    public function includeRole(User $user)
    {

        $role = $user->roles()->strongest();
        return $this->item($role, new RoleTransformer, $role->toString());
    }


}
<?php namespace App\Transformers;

use App\Models\Post;
use League\Fractal;

/**
 * @package App\Transformers
 */
class PostTransformer extends Fractal\TransformerAbstract {
    /**
     * @param Post $post
     * @return array
     */
    public function transform(Post $post)
    {
        return [
            'id'      => (int) $post->id,
            'title'   => $post->title,
            'content' => $post->data,
        ];
    }

    protected $availableIncludes = [
        'category'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'category'
    ];

    public function includeCategory(Post $post)
    {
        $categories = $post->categories();

        return $this->collection($categories->get(), new CategoryTransformer(), $categories->firstOrFail()->toString());
    }
}
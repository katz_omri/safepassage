<?php namespace App\Transformers;

use App\Models\Role;
use League\Fractal;

/**
 * @package App\Transformers
 */
class GenericTransformer extends Fractal\TransformerAbstract {
    /**
     * @param $generic
     * @return array
     */
    public function transform($generic)
    {
        $key = array_keys($generic);
        return [
            'item' => [
                $key[0] => $generic[$key[0]]
            ],
            'objectType' => $generic['type']
        ];
    }
}
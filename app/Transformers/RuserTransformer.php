<?php namespace App\Transformers;


use App\Models\Ruser;

use League\Fractal;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class RuserTransformer extends Fractal\TransformerAbstract {


    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'loginData'
    ];

    public function transform($user)
    {

        return [
            'id'         => (int) $user->id,
            'role'       => $user['role'],
            'email'      => $user->email,
            'last_name'  => $user->last_name,
            'first_name' => $user->first_name
        ];
    }

    /**
     * Include LoginData
     * @param Ruser $ruser
     * @return Fractal\Resource\Item
     */
    public function includeLoginData(Ruser $ruser)
    {
        $loginData = $ruser->loginData()->first();
        if (is_null($loginData)) {
            return;
        }
        return $this->item($loginData, new UserLoginDataTransformer, $loginData->toString());
    }
}
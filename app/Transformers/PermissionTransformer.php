<?php namespace App\Transformers;


/**
 * Created by PhpStorm.
 * User: omri
 * Date: 11/28/15
 * Time: 10:48
 */

use App\Models\Permission;
use League\Fractal;

/**
 *
 * @package App\Transformers
 */
class PermissionTransformer extends Fractal\TransformerAbstract {
    /**
     * @param Permission $permission
     * @return array
     */
    public function transform(Permission $permission)
    {
        return [
            'id'           => (int) $permission->id,
            'name'         => $permission->name,
            'description'  => $permission->description,
            'display_name' => $permission->display_name
        ];
    }
}
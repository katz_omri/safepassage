'use strict';

import config from '../config';
import gulp   from 'gulp';
import notify from 'gulp-notify'
import elixir from 'laravel-elixir';
import watch from 'gulp-watch';

global.isWatching = true;

gulp.task('watch', function() {



    // Scripts are automatically watched and rebundled by Watchify inside Browserify task
    //gulp.watch(config.scripts.src, ['eslint']);
    gulp.watch(config.styles.src,  ['styles']);
    //gulp.watch(config.images.src,  ['images']);
    //gulp.watch(config.fonts.src,   ['fonts']);
    //gulp.watch(config.fonts.src,   ['fonts']);
});
'use strict';

import config from '../config';
import gulp   from 'gulp';
import del    from 'del';
import yargs   from 'yargs';

const argv = yargs.argv;

gulp.task('clean', function() {
    return del([
        config.buildDir,
        config.scripts.version,
        config.styles.version
    ]);
  
});

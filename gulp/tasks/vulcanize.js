/**
 * Created by omri on 4/29/16.
 */

import vulcanize  from 'gulp-vulcanize';
import rename     from 'gulp-rename';
import gulp       from 'gulp';
import config     from '../config';
import htmlmin    from 'gulp-htmlmin';
import crisper    from 'gulp-crisper';
import gulpif     from 'gulp-if';
import gulpfilter from 'gulp-filter';
import uglify     from 'gulp-uglify';


var htmlFilter = gulpfilter('**/*.html', {restore: true});


var jsFilter = gulpfilter('**/*.js', {restore: true});

gulp.task('vulcanize', function () {
    return gulp.src(config.elements.src)

        .pipe(vulcanize({
            abspath: '',
            inlineScripts: true,
            excludes: [],
            stripExcludes: false
        }, config.vulcanize.bundleName))
        .pipe(crisper({
            scriptInHead: false, // true is default
            // config.vulcanize.bundleName
            onlySplit: false
        }))
        .pipe(rename(function(file) {
                if (file.extname === '.html') {
                    file.extname = '';
                    return file.basename = config.vulcanize.bundleName
                }
            }
        ))
        .pipe(htmlFilter)
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyJS: true,
            minifyCSS: true,
            removeComments: true
        }))
        .pipe(htmlFilter.restore)
        .pipe(jsFilter)
        .pipe(uglify({
            compress: { drop_console: true } // eslint-disable-line camelcase
        }))
        .pipe(jsFilter.restore)
        .pipe(gulp.dest(config.elements.dest));
});

'use strict';

import gulp         from 'gulp';
import gulpif       from 'gulp-if';
import source       from 'vinyl-source-stream';
import sourcemaps   from 'gulp-sourcemaps';
import buffer       from 'vinyl-buffer';
import streamify    from 'gulp-streamify';
import watchify     from 'watchify';
import browserify   from 'browserify';
import babelify     from 'babelify';
import uglify       from 'gulp-uglify';
import browserSync  from 'browser-sync';
import debowerify   from 'debowerify';
import ngAnnotate   from 'browserify-ngannotate';
import handleErrors from '../util/handleErrors';
import bundleLogger from '../util/bundleLogger';
import config       from '../config';
import browserifyShim from 'browserify-shim';
import packageJson from '../../package.json';
import bowerJson from '../../public/app/bower.json';
import elixir from 'laravel-elixir';
import rename from 'gulp-rename';



// Based on: http://blog.avisi.nl/2014/04/25/how-to-keep-a-fast-build-with-browserify-and-reactjs/
function buildScript(file) {
    process.env.BROWSERIFYSHIM_DIAGNOSTICS=1
    const shouldCreateSourcemap = !global.isProd || config.browserify.prodSourcemap;
    const scriptDest = global.isProd ? config.scripts.version : config.scripts.dest;
    let dependencies = Object.keys(packageJson && packageJson.dependencies || {});

    let bundler = browserify({
        entries: [config.sourceDir + file],
        debug: shouldCreateSourcemap,
        cache: {},
        packageCache: {},
        plugin: [watchify],
        fullPaths: !global.isProd
    })

    if ( !global.isProd ) {
        bundler = watchify(bundler);
        bundler.on('update', rebundle)


    }

    const transforms = [
        { 'name':babelify, 'options': {
            'presets': ['es2015', 'react']
        }},
        //{ 'name':debowerify, 'options': {preferNPM: true}},
        //{ 'name':ngAnnotate, 'options': {}},
        { 'name':'brfs', 'options': {}},
        //{ 'name':'bulkify', 'options': {}}
    ];

    transforms.forEach(function(transform) {
        bundler.transform(transform.name, transform.options);
    });

    function rebundle() {
        bundleLogger.start();
        dependencies.push('animatescroll');
        bundler.external(dependencies);
        let stream = bundler.bundle();

        let sourceMapLocation =  '';

        return stream
            .pipe(source(file))
            .pipe(gulpif(shouldCreateSourcemap, buffer()))
            .pipe(gulpif(shouldCreateSourcemap, sourcemaps.init({ loadMaps: true })))
            .pipe(gulpif(global.isProd, streamify(uglify({
                compress: { drop_console: true } // eslint-disable-line camelcase
            }))))
            .pipe(gulpif(shouldCreateSourcemap, sourcemaps.write(sourceMapLocation)))
            .pipe(rename(config.browserify.bundleName))
            .pipe(gulp.dest(scriptDest))
        .pipe(browserSync.stream());


    }

    function reVersion() {

        elixir(function(mix) {
            mix.version([
                config.scripts.dest + config.browserify.bundleName,
                config.scripts.dest + config.browserify.vendorName,
            ], 'build');
        });
    }

    return rebundle();

}

gulp.task('browserify', function() {

    global.isWatching = true;


    return buildScript('entry.js');

});

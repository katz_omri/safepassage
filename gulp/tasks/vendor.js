import gulp         from 'gulp';
import gulpif       from 'gulp-if';
import source       from 'vinyl-source-stream';
import sourcemaps   from 'gulp-sourcemaps';
import buffer       from 'vinyl-buffer';
import streamify    from 'gulp-streamify';
import watchify     from 'watchify';
import browserify   from 'browserify';
import babelify     from 'babelify';
import uglify       from 'gulp-uglify';
import browserSync  from 'browser-sync';
import debowerify   from 'debowerify';
import ngAnnotate   from 'browserify-ngannotate';
import handleErrors from '../util/handleErrors';
import bundleLogger from '../util/bundleLogger';
import config       from '../config';
import browserifyShim from 'browserify-shim';
import packageJson from '../../package.json';

gulp.task('vendor', function () {
    return vendorScripts();
});
const shouldCreateSourcemap = !global.isProd || config.browserify.prodSourcemap;

let bundler = browserify({
    debug: shouldCreateSourcemap,
    cache: {},
    packageCache: {}
})

const transforms = [
    { 'name':debowerify, 'options': {preferNPM: true}},
    //{ 'name':ngAnnotate, 'options': {}},
    //{ 'name':'brfs', 'options': {}},
    //{ 'name':'bulkify', 'options': {}}
];


function vendorScripts() {
    const scriptDest = global.isProd ? config.scripts.version : config.scripts.dest;

    transforms.forEach(function(transform) {
        bundler.transform(transform.name, transform.options);
    });
    const shouldCreateSourcemap = !global.isProd || config.browserify.prodSourcemap;
    let sourceMapLocation = global.isProd ? './' : '../maps';
    let dependencies = Object.keys(packageJson && packageJson.dependencies || {});

    dependencies.push('animatescroll');

    return bundler
        .require(dependencies)
        .bundle()
        .pipe(source('vendor.js'))
        .pipe(gulpif(shouldCreateSourcemap, buffer()))
        .pipe(gulpif(shouldCreateSourcemap, sourcemaps.init({ loadMaps: true })))
        .pipe(gulpif(global.isProd, streamify(uglify({
            compress: { drop_console: true } // eslint-disable-line camelcase
        }))))
        .pipe(gulpif(shouldCreateSourcemap, sourcemaps.write(sourceMapLocation)))
        .pipe(gulp.dest(scriptDest))
}

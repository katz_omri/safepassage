'use strict';

import config from '../config';
import gulp   from 'gulp';
import del    from 'del';

/*
 wrapper for version, so we can clean up the folders.
 */
gulp.task('finish', ['version'], function() {
    return del([config.scripts.version, config.styles.version]);
});

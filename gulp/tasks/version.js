/**
 * Created by omri on 4/29/16.
 */

import fs          from 'fs';
import gulp        from 'gulp';
import elixir      from 'laravel-elixir';
import config      from '../config';

gulp.task('version', function() {
    let bundle = [
        config.scripts.version + config.browserify.bundleName,
        config.scripts.version + config.browserify.vendorName,
        config.styles.version  + config.styles.bundleName + '.min.css'
    ];
    if ( ! global.isProd) {
        scripts = [];
    }
    elixir(function(mix) {
        mix.version(bundle)
    });

});

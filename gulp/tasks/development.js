'use strict';

import gulp        from 'gulp';
import runSequence from 'run-sequence';
import elixir from 'laravel-elixir';
import config from '../config';
import watch from 'gulp-watch';



gulp.task('dev', ['clean'], function(cb) {


    cb = cb || function() {

    };

    global.isWatching = true;
    global.isProd = false;

    runSequence( ['vendor', 'vulcanize', 'styles','browserify'], 'watch', cb);
    //gulp.watch(config.styles.src,  ['styles']);

});

'use strict';

import gulp        from 'gulp';
import runSequence from 'run-sequence';
import del         from 'del';
import config      from '../config';

gulp.task('prod', ['clean'], function(cb) {

  cb = function() {
      //return del([config.scripts.version]);
  };

  global.isProd = true;

  // runSequence( ['vendor', 'browserify', 'styles', 'vulcanize'],'version', cb);
    runSequence( ['vendor','vulcanize', 'browserify', 'styles'], 'finish', cb);

});
//gulp.task('prod', ['clean'], function(cb) {
//
//  cb = cb || function() {};
//
//  global.isProd = true;
//
//  runSequence(['styles', 'images', 'fonts', 'views'], 'browserify', 'gzip', cb);
//
//});

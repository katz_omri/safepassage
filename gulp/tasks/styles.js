'use strict';

import config       from '../config';
import gulp         from 'gulp';
import gulpif       from 'gulp-if';
import sourcemaps   from 'gulp-sourcemaps';
import sass         from 'gulp-sass';
import handleErrors from '../util/handleErrors';
import browserSync  from 'browser-sync';
import autoprefixer from 'gulp-autoprefixer';
import rename       from 'gulp-rename';

gulp.task('styles', function () {

    const createSourcemap = !global.isProd || config.styles.prodSourcemap;
    const stylesDest = global.isProd ? config.styles.version : config.styles.dest;

    return gulp.src(config.styles.src)
        .pipe(gulpif(createSourcemap, sourcemaps.init()))
        .pipe(sass({
            sourceComments: !global.isProd,
            outputStyle: global.isProd ? 'compressed' : 'nested',
            includePaths: config.styles.sassIncludePaths
        }))
        .on('error', handleErrors)
        .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 8'))
        .pipe(gulpif(
            createSourcemap,
            sourcemaps.write( global.isProd ? './' : null ))
    )
        .pipe(rename(function(file) {
            file.basename = config.styles.bundleName;
        }))
        .pipe(gulpif(
            global.isProd,
            rename(function(file) {
            file.extname = '.min.css';
        })))
        .pipe(gulp.dest(stylesDest))
        .pipe(browserSync.stream());

});

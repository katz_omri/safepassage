'use strict';

export default {

    browserPort: 3000,
    UIPort: 3001,
    testPort: 3002,

    sourceDir: './public/app/scripts/',
    buildDir: './public/build/',

    styles: {
        src: './public/app/styles/sass/**/*.scss',
        version: './public/css/',
        bundleName: 'main',
        dest: './public/build/styles',
        prodSourcemap: false,
        sassIncludePaths: []
    },

    elements: {
        src: './public/app/elements/elements.html',
        version: './public/elements/',
        dest: './public/build/elements',
        test: '/public/test/**/*.js',
        gulp: './gulp/**/*.js'
    },

    scripts: {
        src: './public/app/js/**/*.js',
        version: './public/js/',
        dest: './public/build/js/',
        test: 'test/**/*.js',
        gulp: 'gulp/**/*.js'
    },

    images: {
        src: 'public/images/**/*',
        dest: 'build/images'
    },

    fonts: {
        src: ['app/fonts/**/*'],
        dest: 'build/fonts'
    },

    assetExtensions: [
        'js',
        'css',
        'png',
        'jpe?g',
        'gif',
        'svg',
        'eot',
        'otf',
        'ttc',
        'ttf',
        'woff2?'
    ],

    views: {
        index: 'app/index.html',
        src: 'app/views/**/*.html',
        dest: 'app/js'
    },

    gzip: {
        src: 'build/**/*.{html,xml,json,css,js,js.map,css.map}',
        dest: 'build/',
        options: {}
    },
    vulcanize: {
        bundleName: 'elements.vulcanized.html',
        jsFileName: 'elements.js'
    },
    browserify: {
        bundleName: 'common.js',
        vendorName: 'vendor.js',
        prodSourcemap: false,
        exclude: [
            "angular",
            "angular-local-storage",
            "angular-material",
            "angular-resource",
            "angular-storage",
            "angular-ui-router",
            "jquery"
        ]
    },
    debowerify: {
        bundleName: 'vendor.js'
    },


    init: function() {
        this.views.watch = [
            this.views.index,
            this.views.src
        ];

        return this;
    }

}.init();

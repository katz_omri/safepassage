<?php



return [


    'role' => App\Models\Role::class,


    'roles_table' => 'role',


    'permission' => App\Models\Permission::class,


    'permissions_table' => 'permissions',


    'permission_role_table' => 'permission_role',


    'role_user_table' => 'role_user',

];

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'login_email' => $faker->email,
        'last_name' => $faker->lastName,
        'password' => bcrypt('password'),
        'address' => $faker->address,
        'city' =>$faker->city,
        'country' => $faker->country,
        'phone' => $faker->phoneNumber
    ];
});
$factory->defineAs(App\Models\User::class, 'admin', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);
    return array_merge($user, [
        'password'    => bcrypt(env('ADMIN_PASSWORD')),
        'login_email' => env('ADMIN_EMAIL')
    ]);
});

$factory->define(App\Models\Ruser::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'email' => $faker->email,
        'last_name' => $faker->lastName,
        'address' => $faker->address,
        'city' =>$faker->city,
        'role' => 'viewer',
        'country' => $faker->country,
        'phone' => $faker->phoneNumber
    ];
});
$factory->defineAs(App\Models\Ruser::class, 'custom', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\Ruser::class);
    return array_merge($user, ['email' => 'user@gmail.com']);
});

$factory->define(App\Models\UserLoginData::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'login_email' => $faker->email,
        'last_name' => $faker->lastName,
        'password' => bcrypt('password')
    ];
});
$factory->defineAs(App\Models\UserLoginData::class, 'custom', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\UserLoginData::class);
    return array_merge($user, ['login_email' => 'user@gmail.com']);
});
$factory->define(App\Models\Sponsor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->email,
        'representative_name'  => $faker->name,
        'sponsorship_quota' => $faker->randomNumber(5),
        'number_of_members' => $faker->randomNumber(2),
        'address' => $faker->address,
        'city' => $faker->city,
        'country' => $faker->country,
        'phone' => $faker->phoneNumber,
        'previous_experience' => 1,
        'status' => 1,
    ];
});



<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_login_data');
        Schema::create('user_login_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login_email')->index();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('status');
            $table->string('authenticatable_id')->index();
            $table->string('authenticatable_type');
            $table->string('password', 60);
            $table->text('custom_data')->nullable();
            $table->rememberToken();
            $table->boolean('active')->default('0');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_login_data');
    }
}

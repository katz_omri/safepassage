<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersSeeder
 */
class RoleSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'viewer',
            'display_name' => 'viewer role',
            'description' => 'Base role for newly created users'
        ]);
        Role::create([
            'name' => 'refugee',
            'display_name' => 'Refugee',
            'description' => 'Refugees assigned or unassigned to sponsors'
        ]);
        Role::create([
            'name' => 'sponsor',
            'display_name' => 'Sponsor Group',
            'description' => 'Sponsors'
        ]);
        Role::create([
            'name' => 'admin',
            'display_name' => 'Administrators',
            'description' => 'Site managers'
        ])->givePermissionsTo(Permission::all());

    }





}

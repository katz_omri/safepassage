<?php

use App\Models\Sponsor;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RuserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = \Faker\Factory::create();
        $users = factory(App\Models\Ruser::class, 5)->create();
        $numberSponsors = Sponsor::all()->count();
        foreach ($users as $user) {
            $user->sponsor()->associate($faker->numberBetween(1, $numberSponsors -1))->save();
            $user->enableLogin(factory(App\Models\UserLoginData::class, 1)->create([
                'login_email' => $user->email,
                'password' => bcrypt('password'),
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            ]));
        }

    }
}
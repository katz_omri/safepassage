<?php
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 2/19/16
 * Time: 13:37
 */

use App\Models\Category;
use Illuminate\Database\Seeder;

/**
 * Class UsersSeeder
 */
class CategorySeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'home',
            'display_name' => 'Home'
        ]);
        Category::create([
            'name' => 'projects',
            'display_name' => 'Projects'
        ]);
        Category::create([
            'name' => 'SAP',
            'display_name' => 'Sponsorship Assistance Project'
        ]);
        Category::create([
            'name' => 'REW',
            'display_name' => 'Resettlement Empowerment Workshop'
        ]);
        Category::create([
            'name' => 'about',
            'display_name' => 'About us'
        ]);
        Category::create([
            'name' => 'sponsor',
            'display_name' => 'Sponsor'
        ]);
        Category::create([
            'name' => 'top',
            'display_name' => 'Top'
        ]);
        Category::create([
            'name' => 'middle',
            'display_name' => 'Middle'
        ]);
        Category::create([
            'name' => 'bottom',
            'display_name' => 'Bottom'
        ]);
        Category::create([
            'name' => 'mission',
            'display_name' => 'Mission'
        ]);
    }





}

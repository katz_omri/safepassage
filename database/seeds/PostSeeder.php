<?php

use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersSeeder
 */
class PostSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Post::create([
            'title' => 'Matching refugees to sponsors',
            'data'  => 'Safe Passage invites sponsorship groups that are seeking refugees to sponsor to fill out the Refugee Sponsorship “Pairing” form. This enables Safe Passage caseworkers to pair refugees seeking sponsors in Jordan with sponsorship groups in Canada, according to the groups’ sponsorship capacity.',
            'image' => 'app/images/refugee.png',
            'type'  => 'card'
        ])->assignCategory('projects', 'top', 'SAP');
        Post::create([
            'title' => 'Assisting sponsorship applicants',
            'data'  => 'Refugee sponsorship applicants, whose sponsorship has been confirmed by sponsorship groups, receive ongoing assistance through one-on-one meetings with English and Arabic Safe Passage Caseworkers in Jordan. This includes assistance in completing sponsorship application forms, as well as guidance concerning the subsequent steps of the application process.',
            'image' => 'app/images/cover_2.png',
            'type'  => 'card'
        ])->assignCategory('projects', 'top', 'SAP');
        Post::create([
            'title' => 'Providing a secure online platform',
            'data'  => 'Each refugee sponsorship applicant has a secure profile, embedded in the secure online platform, accessible only to the relevant parties in the sponsorship process. The profile allows the progress of the application to be tracked and the completed applications to be uploaded by the Safe Passage caseworker in Jordan for review by the sponsor in Canada.',
            'image' => 'app/images/cover_3.png',
            'type'  => 'card'
        ])->assignCategory('projects', 'top', 'SAP');

        Post::create([
            'title'    =>   'About Safe Passage Canada',
            'data'     =>   '<p>Safe Passage Canada aims to facilitate the private sponsorship of refugees process by <b>serving as a bridge </b>between sponsors in Canada and refugees in Jordan.
            </p>',
            'type'     =>   'section'
        ])->assignCategory('about', 'top');

        Post::create([
            'data'     =>   '<p>For the first time, Safe Passage Canada will <b>assist sponsors and refugees directly from Jordan,</b> enabling sponsors, refugees and their Safe Passage Canada caseworkers to collaborate via our <b>online platform </b> and have access to on-the-ground, ongoing support.</p>',
            'type'     =>   'section'
        ])->assignCategory('home', 'bottom');
        Post::create([
            'data'     => 'As of September 20th 2015, Community sponsors and Groups of Five no longer have to provide proof that the person they are sponsoring has been recognized as a refugee. The requirement for refugee status recognition has been waived for Iraqi and Syrian refugee applicants.',
            'type'     => 'section'
        ])->assignCategory('sponsor', 'middle');
        Post::create([
            'title'    => 'Our Mission',
            'data'     => '<p>
                            To empower refugees in Jordan and their sponsors in Canada throughout the Canadian resettlement process by:
                            </p>
                            <ul class="fa-ul">
                                <li><i class="fa-li fa fa-check"></i>Connecting refugees and sponsors through our online platform and provide on the ground, ongoing casework support to refugees in Jordan.</li>
                                <li><i class="fa-li fa fa-check"></i>Increasing the resettlement opportunities of refugees in Jordan by pairing vulnerable refugees with willing Canadian sponsors.</li>
                                <li><i class="fa-li fa fa-check"></i>Preparing refugees for their life in Canada by providing language training and pre-departure cultural orientation and integration workshops.</li>
                            </ul>
                            ',
            'type'     => 'article'
        ])->assignCategory('about', 'middle', 'mission');
        Post::create([
            'title'    => 'What we do',
            'data'     => 'Safe Passage transcends administrative and physical boundaries, enhancing the efficiency of the sponsorship process through a three-tiered approach',
            'type'     => 'section'
        ])->assignCategory('home', 'middle');
        Post::create([
            'title' => 'The growing refugee crisis',
            'data'  => '
                <p>
                    The ongoing civil war in Syria has resulted in millions of people seeking refuge in neighboring countries. Since the onset of the crisis, Jordan has accepted Syrian refugees, hosting individuals and families within Jordanian communities, as well as in refugee camps, such as Za’atari and Azraq. Currently 1.4 million Syrian refugees, of which 635,000 have been registered by the UNHCR, reside in Jordan.
                </p>
                <p>
                    Concurrently, Jordan has seen a resurgence of Iraqi refugees entering the Country. As of July 2015, 30,000 Iraqis have registered with the UNHCR. This new wave of refugees, the majority of whom are fleeing the threat of ISIL, is in addition to the pre-existing numbers of Iraqis who fled during the Gulf War, and the subsequent 2003 Iraq invasion.
                </p>
                <p>
                    The Syrian crisis has dramatically affected Jordan’s capacity for administering effective refugee relief. The Jordanian government has allocated one quarter of its state budget in support of refugees within their country, which include Syrians, Iraqis and Palestinians. Refugees in Jordan account for 20 percent of the total population, and present a serious burden on Jordan’s infrastructure. The impact on Jordan and the ongoing crises in Syria and Iraq has prompted the UNHCR to regard third-party resettlement as the most durable solution for these populations.
                </p>
                '
        ])->assignCategory('about','middle');
        Post::create([
            'title' => 'Canada’s response',
            'data'  => '<p>
                            The severity of the refugee crisis has warranted strong support for refugee resettlement among Canadian citizens and political leaders. Organizations and groups of Canadians across the country are contributing significant time and resources to resettle refugees through the Private Sponsorship of Refugees Program.
                        </p>
                        <p>
                        In an effort to ease the sponsorship process, the Canadian government temporarily waived the requirement for Syrian and Iraqi refugees to obtain refugee status, which under usual circumstances, is a standard prerequisite. Since the inception of the Private Sponsorship of Refugees Program in 1979, over 275,000 refugees have been successfully resettled to Canada.
                        </p>'
        ])->assignCategory('about', 'middle');
        Post::create([
            'title' => 'Obstacles in the sponsorship process',
            'data'  => '
                <p>
                    While sponsors are working toward a common goal, there remain obstacles throughout the application process. Some of the major setbacks are directly related to the absence of assistance provided to refugee applicants in their host country, and communication difficulties between multiple parties (i.e. sponsorship organizations, co-­sponsors, refugee applicants, overseas organizations) partaking in the process. Furthermore, there is a lack of local government assistance provided to sponsors in regards to submitting the burdensome paperwork without errors, and an overall absence of a single entity that mobilizes and coordinates all the components in the application process.
                </p>
            '
        ])->assignCategory('about', 'bottom');
        Post::create([
            'title' => 'What is Canadian Sponsorship',
            'data' => '
                            <p>
                            Canadian Private Sponsorship is a unique process through which sponsoring groups, composed of groups of individuals and/or organizations, initiate and provide the financial and material support to resettle refugees to Canada. Most often, sponsorship groups identify the individual refugees they seek to sponsor, but sponsors are not necessarily required to have a connection to the refugees they are sponsoring.</p><br>
                            <p>
                            The private sponsorship process entails different steps and requires involvement from both the sponsors and the refugee sponsorship applicants. Firstly, at the Canadian end of the process, sponsors must complete a set of application forms, included in the <a target="_blank" href="http://www.cic.gc.ca/english/information/applications/private.asp"> Application for Refugee Sponsorship</a>, and sign a sponsorship undertaking.
                            </p>
                            <br>
                            <p>
                            Secondly, refugee sponsorship applicants complete an Application for Permanent Residence, which must be returned to Canada and included in the same application package. The completed application package, containing both the sponsors and the refugee applicant’s forms, is then sent to the Centralized Processing Office in Winnipeg, where the sponsorship undertaking is approved and a file number is provided to the applicants. The applications are then sent overseas to the relevant Canadian visa office for further processing
                            </p>
                            <br>
                            <p>
                            The refugee applicants are assessed for eligibility and admissibility at the overseas visa office. Upon arrival to Canada, refugee applicants receive permanent residence and sponsorship groups are responsible for providing the necessary orientation, settlement and emotional support, allowing the refugees to become self-supporting, typically within a one year period.
                            </p>'
        ])->assignCategory('sponsor');
        Post::create([
            'title' => 'Who can sponsor',
            'data' => '
                <p>
                Sponsorship groups vary in composition. Many sponsorship groups include individuals (citizens or permanent residents), organisations and associations, or a combination of these elements. The following types of groups are recognized by the government as being eligible to sponsor refugees:
                </p>
                <br>

                <h4>Sponsorship Agreement Holder </h4>
                <p>
                A Sponsorship Agreement Holder (SAH) is an organization that has been recognized by the Canadian Government, through a Sponsorship Agreement, as being able to sponsor refugees. A SAH may directly sponsor refugees, or may choose to work through Constituent Groups that sponsor refugees as part of the SAH’s agreement. A list of Sponsorship Agreement Holders, per province, is available <a target="_blank" href="http://www.cic.gc.ca/english/refugees/sponsor/list-sponsors.asp">here.</a>
                </p>
                <br>

                <h4>Group of Five </h4>
                <p>
                Group of Five sponsorship allows a group of five or more individuals, composed of Canadian citizens or permanent residents, to collectively sponsor a refugee living abroad. Each of the sponsors acts as a guarantor in committing to provide the necessary support for the duration of the sponsorship period. More information about Group of Five sponsorship, including how to form a Group of Five, is available on the <a href="http://www.cic.gc.ca/english/refugees/sponsor/community.asp" target="_blank">Canadian Immigration and Citizenship Website. </a>

                </p>
                <br>

                <h4>
                Community Sponsorship
                </h4>
                <p>
                A Community sponsor can be any organization, association, corporation, etc. that sponsors a refugee living abroad. The community sponsor acts as a guarantor in committing to provide the necessary support for the duration of the sponsorship period. The community sponsor has the option to share their responsibilities by forming a partnership with co-sponsors, such as another organization or an individual. More information about Community Sponsorship is available on the <a target="_blank" href="http://www.cic.gc.ca/english/refugees/sponsor/community.asp">Canadian Immigration and Citizenship Website.  </a>
                </p>
                <br>
                <h4>
                Blended Visa Office Referred Program
                </h4>
                <p>
                The Blended Visa Office Referred Program is the joint sponsorship of a refugee through both private and government sponsorship. The private sponsor must be a SAH or a Constituent Group, and provides six months of financial support, while the Government of Canada provides income support for the remaining six months. This sponsorship program is conducted through the UNHCR, and its processing method is not applicable to Safe Passage Canada
                </p>'
        ])->assignCategory('sponsor');

        Post::create([
            'title' => 'Who can be sponsored',
            'data' => '<p>
                Refugees and persons in refugee-like situations are eligible for Canadian sponsorship. This refers to the two distinct classes; the Convention Refugees Abroad Class and the Country of Asylum Class.
                </p>
                <br>
                <p>
                According to Citizenship and Immigration Canada (CIC):
                </p>
                <br>

                <b>A Convention refugee</b>
                <p>  is any person who by reason of a well-founded fear of persecution because of race, religion, nationality, membership in a particular social group or political opinion:
                 </p>
                 <br>
                 <ul>
                 <li>Is outside the country of his or her nationality and is unable or, by reason of that fear, unwilling to avail himself or herself of the protection of that country; or </li>
                 <li>Does not have a country of nationality, is outside the country of his or her former habitual residence and is unable or, by reason of that fear, unwilling to return to that country;</li>
                 <li>Does not have a prospect of another durable solution, within a reasonable period of time</li>
                 </ul>
                 <br>

                <p>
                 A member of the <b> Country of Asylum Class </b> is a person:
                 </p>
                 <br>
                 <ul>
                 <li>who is outside his or her country of citizenship or habitual residence;</li>
                 <li>ho has been, and continues to be, seriously and personally affected by civil war or armed conflict or who has suffered massive violations of human rights;
                </li>
                <li>for whom there is no possibility of finding an adequate solution to his or her situation within a reasonable period of time
                </li>
                 </ul> '
        ])->assignCategory('sponsor');

    }



}

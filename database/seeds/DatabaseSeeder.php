<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(PermissionSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RuserSeeder::class);
        $this->call(UserLoginDataTableSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(SponsorSeeder::class);
        Model::reguard();
    }
}

<?php

use App\Models\Sponsor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersSeeder
 */
class UserSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = \Faker\Factory::create();
        $users = factory(App\Models\User::class, 5)->create();
        $users[] = factory(App\Models\User::class, 'admin', 1)->create();
        foreach ($users as $user) {
            $user->sponsor()->associate($faker->numberBetween(1, 4))->save();
        }


    }


}

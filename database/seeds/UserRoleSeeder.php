<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersSeeder
 */
class UserRoleSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new App\Models\User;
        $users = $user->all();
        foreach ($users as $user) {
            $user->assignRole('viewer');
        }
        $user = $user->where('login_email', env('ADMIN_EMAIL'))->firstOrFail();
        $user->assignRole('admin');

    }



}

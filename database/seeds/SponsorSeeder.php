<?php

use App\Models\Sponsor;
use App\Models\UserLoginData;
use Illuminate\Database\Seeder;


/**
 * Class UsersSeeder
 */
class SponsorSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     *
     */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sponsors = factory(App\Models\Sponsor::class, 5)->create([
            'type' => App\Models\Sponsor::getTypes()[1]
        ]);
        foreach ($sponsors as $sponsor) {
            $users = factory(App\Models\User::class, 10)->make();
            $rusers = factory(App\Models\Ruser::class, 10)->make();
            foreach ($users as $user) {
                $sponsor->addUser($user)
                    ->assignRole('sponsor');
            }
            foreach($rusers as $ruser) {
                $sponsor->addRuser($ruser);
            }

        }
    }




}

<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersSeeder
 */
class PermissionSeeder extends Seeder {
    /**
     *
     */
    /**
     * @var
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'read',
            'display_name' => 'Read Permissions',
            'description' => 'Read permissions for all'
        ]);
        Permission::create([
            'name' => 'write',
            'display_name' => 'Write',
            'description' => 'Write permissions'
        ]);
        Permission::create([
            'name' => 'publish',
            'display_name' => 'Publish articles',
            'description' => 'Publish articles for Site managers'
        ]);
        Permission::create([
            'name' => 'register',
            'display_name' => 'Register Refugees',
            'description' => 'Register Refugees for sponsors'
        ]);

    }




}

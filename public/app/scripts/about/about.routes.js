/**
 * Created by omri on 2/19/16.
 */

module.exports = ['$stateProvider', States]
function States($stateProvider) {
    $stateProvider.state('about', {
        url: '/about',
        templateUrl: 'app/scripts/about/about.html',
        controller: 'AboutController',
        controllerAs: 'about',
        category: 'about',
        abstract: true,
        data: {
            visible: false
        }
    })
        .state('about.main', {
            url: '',
            data: {
                menuIndex: 3,
                visible: true,
                url: '/about',
                name: 'About us',
                menu: ['main', 'sidenav'],
                requiresLogin: false
            }
        })
    

}



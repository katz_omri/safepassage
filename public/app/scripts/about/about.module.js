
import AboutController from './about.controller';
import routes          from './about.routes';


angular.module('app.about', [])
    .controller('AboutController', AboutController)
    .config(routes);

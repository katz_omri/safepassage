

module.exports = ['$rootScope', 'api', 'logger','$timeout', AboutController];


function AboutController($rootScope, api, logger, $timeout) {


    var vm = this;
    vm.fetched = false;
    vm.team = {};
    vm.posts = {};
    vm.categories = {};

    activate();

    function activate() {
        logger.info('AboutController: activating...');
        _getData();
        _getTeam();
    }
    function _getData() {
        vm.fetching = true;
        return api.posts('about')
            .then(function(data) {
                    vm.posts = data.data;
                    vm.fetching = false;
                    logger.info('setting about.posts: ', data.data);
                    vm.categories = data.included;
                    logger.info('setting about.categories: ', data.included);

            });
    }
    function _getTeam() {
        return api.getJson('team')
            .then(function(response) {
                vm.team = response.team;
                logger.info('setting about.team: ', response.team);
        });
    }



}


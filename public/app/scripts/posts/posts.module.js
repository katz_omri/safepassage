
import Controllers     from './controllers/posts.controllers.module';
import routes          from './posts.routes';


angular.module('app.posts', [
    'app.posts.controllers'
    ])
    .config(routes);

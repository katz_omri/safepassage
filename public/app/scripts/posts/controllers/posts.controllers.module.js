/**
 * Created by omri on 1/9/16
 *
 * Define the general auth controllers module.
 * Other controllers for auth will bind to this module.
 */


import PostsMainController from './posts.main.controller';
import PostsEditController from './posts.edit.controller';

module.exports = angular.module('app.posts.controllers', [])
    .controller('PostsMainController', PostsMainController)
    .controller('PostsEditController', PostsEditController);


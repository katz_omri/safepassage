
module.exports = [
    '$rootScope',
    'api',
    'logger',
    '$stateParams',
    'utils',
    PostsEditController];


function PostsEditController($rootScope, api, logger, $stateParams, utils) {
    var vm = this;
    vm.post = {};
    vm.savePost = savePost;

    activate();

    function activate() {
        logger.log('posts.EditController: activated');
        vm.post = $stateParams.data;
        if ( ! vm.post && $stateParams.postId) {
            api.posts().then(function(data) {
                vm.post = utils.findById(data.data, $stateParams.postId);
                logger.log('PostsEditController: no postData in $stateParams, setting vm.post using api: ', vm.post);
            });
        }
    }

    function savePost() {

        api.post('posts/' + vm.post.id, {
            data: tinymce.activeEditor.getContent({format: 'raw'}),
            title: vm.post.attributes.title
        }).then(function() {
        })
    }



}


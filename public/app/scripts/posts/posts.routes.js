/**
 * Created by omri on 2/19/16.
 */

module.exports = ['$stateProvider', States]
function States($stateProvider) {
    $stateProvider.state('posts', {
        url: '/posts',
        templateUrl: 'app/scripts/posts/posts.html',
        controller: 'PostsMainController',
        controllerAs: 'vm',
        category: 'main',
        abstract: true,
        data: {
            visible: false
        }
    })
        .state('posts.view', {
            url: '/posts',
            data: {
                menuIndex: 3,
                visible: false,
                url: '/posts',
                name: 'Posts',
                menu: ['main', 'sidenav'],
                requiresLogin: false
            }
        })
        .state('posts.edit', {
            url: '/:postId/edit',
            controller: 'PostsEditController',
            controllerAs: 'vm',
            params: {
                data: null
            },
            templateUrl: 'app/scripts/posts/posts.view.edit.html',
            data: {
                menuIndex: 3,
                visible: false,
                url: '/posts',
                name: 'Posts',
                menu: ['main', 'sidenav'],
                requiresLogin: false
            }
        })
    

}



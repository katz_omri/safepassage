/**
 * Created by omri on 4/29/16.
 */

import angular            from 'angular';
import ngAnimate          from 'angular-animate';
import uiRouter           from 'angular-ui-router';
import LocalStorageModule from 'angular-local-storage';
import ngResource         from 'angular-resource';


global.jQuery = require("jquery");
global.$ = global.jQuery;
var animateScroll = require('animatescroll');
var swal = require ('sweetalert');


/**
 * Created by omri on 4/24/16.
 */

import vendor      from './vendor';
import about       from './about/about.module';
import auth        from './auth/auth.module';
import core        from './core/core.module';
import dashboard   from './dashboard/dashboard.module';
import home        from './home/home.module';
import projects    from './projects/projects.module';
import sponsorship from './sponsorship/sponsorship.module';
import widgets     from './widgets/widgets.module';
import posts       from './posts/posts.module';


const requires = [
    'app.core',
    'app.about',
    'app.widgets',
    'app.projects',
    'app.sponsorship',
    'app.home',
    'app.auth',
    'app.dashboard',
    'app.posts'
];


String.prototype.implode =
    function(number) {
        var strings = [];
        number = this.length > number ? number : 40;

        var temp = this.substr(0, number);
        number = temp.lastIndexOf(' ');
        strings.push(this.substr(0, number));
        strings.push('...');
        strings.push(this.substr(number));
        return strings;
    };

String.prototype.trunc =
    function(n,useWordBoundary){
        var toLong = this.length>n,
            s_ = toLong ? this.substr(0,n-1) : this;
        s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
        return  toLong ? s_ + '...' : s_;
    };


document.addEventListener('WebComponentsReady', function() {


    //angular.bootstrap(document, ["app"]);

    // Perform some behaviour
});

angular.module('app', requires);
var element = angular.element(document);
//This will be truthy if initialized and falsey otherwise.
var isInitialized = element.injector();
if (!isInitialized) {
    angular.bootstrap(element, ['app']);
}
//    .constant('GLOBAL_CONFIG', {
//        'api_url': "http://safepassage.dev.com"
//    })
//    .constant('WOW')
//    .run(['$rootScope', '$state', '$stateParams', 'authService',
//        function($rootScope, $state, $stateParams, authService) {
//            $rootScope.$state = $state;
//            $rootScope.$stateParams = $stateParams;
//            //$rootScope.$on('$stateChangeStart', function(e, to) {
//            //    if (to.data && to.data.requiresLogin) {
//            //        authService.user().then(function(user) {
//            //        }, function(error) {
//            //            $state.go('auth.login');
//            //        })
//            //    }
//            //});
//        }
//    ]);

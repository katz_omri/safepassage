/**
 * Created by omri on 8/15/15.
 */

import routes          from './auth.routes';
import directives      from './directives/auth.directives.module';
import authService     from './AuthService';
import authControllers from './controllers/auth.controllers.module';


angular.module('app.auth', [
    'app.auth.directives',
    'app.auth.controllers',
])
    .service('authService',authService)
    .config(routes);


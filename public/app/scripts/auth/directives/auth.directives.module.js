/**
 * Created by omri on 12/11/15.
 */


import spRegister from './sp-register'
/**
 * Define the general auth directive module.
 * Other directives for auth will bind to this module.
 */


module.exports = angular.module('app.auth.directives', [])
    .directive('spRegister', spRegister);

module.exports = spRegister;


function spRegister() {
    var directive = {
        link: ['$scope', 'element' , 'attrs', 'auth', link],
        templateUrl: 'app/scripts/auth/view/auth.register.sponsor.html',
        controller: ['$scope', registerController],
        controllerAs: 'auth',
        restrict: 'AEC'
    };
    return directive;

    function link(scope, element, attrs, auth) {
        scope.types = [
            'Sponsorship Agreement Holder',
            'Community sponsor',
            'Group of five',
            'Group of two',
            'Non-profit legal person'
        ];
    }
}

function registerController() {
    this.types = [
        'Sponsorship Agreement Holder',
        'Community sponsor',
        'Group of five',
        'Group of two',
        'Non-profit legal person'
    ];
}

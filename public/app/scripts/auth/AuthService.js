/**
 * Created by omri on 8/22/15.
 */

module.exports = ['$q', '$rootScope','api', 'logger', 'SPAPP_CONFIG', authService];




/**
 * Login service
 * Uses embedded, hard-coded data model; acts asynchronously to simulate
 * remote data service call(s).
 *
 * @returns
 * @constructor
 */

function authService($q, $rootScope, api, logger, SPAPP_CONFIG) {
    $rootScope.auth = false;
    var _user = [];
    var service = {
        login: login,
        logout: logout,
        isAdmin: isAdmin,
        authenticate: authenticate,
        user: user,
        clear: clear,
        _user: _user,
        role: role,
        reset: reset,
        authenticated: authenticated
    }


    return service;

    function isAdmin() {
        var defer = $q.defer();
        service.user().then(function(user) {
            service.role(user).then(function(role) {
                if (role === 'admin') {
                    defer.resolve(true);
                } else {
                    defer.reject(false);
                }
            });
        }, function() {
            defer.reject(false);
        });
        return defer.promise;
    }

    function login(user) {
        var defer = $q.defer();
        api.post('auth/login', user).then(function(response) {

            if (response.data.type === 'User' || response.data.type === 'Ruser') {
                prepareUser(response);
                service.clear();
                // User is okay to login, resolve the promise with the logged in user data.
                defer.resolve(response);
            } else {
                // Credentials were not right, reject the promise with the errors.
                defer.reject(response);
            }
        });
        return defer.promise;

    }

    function reset(user) {
        var defer = $q.defer();
        api.post('password/email', user).then(function(response) {
            if (response === 'success') {
                defer.resolve(response);
            } else {
                defer.reject(response);
            }
        });
        return defer.promise;
    }

    // add the role function to the user response.
    // get the role of the user.
    function prepareUser(response) {
        response.role = role;
        return response;
    }

    function role(user) {
        var defer = $q.defer();
        var role = '';
        if (user.hasOwnProperty('included')) {
            user.included.forEach(function(item) {
                if (item.type === 'Role') {
                    role = item.attributes.name;
                }
            });
        }
        if (user.data.attributes.role) {
            role = user.data.attributes.role;
        }
        defer.resolve(role);
        return defer.promise;
    }

    function authenticate() {
        return this.user().then(function(user) {
            $rootScope.user = user;
        }, function() {
            $rootScope.user = 'anonymous';
        });
    }
    function clear() {
        angular.forEach(service, function(value, key) {
            if ( key.indexOf('_') !== -1 ) {
                logger.info('reset key in auth service: ', key);
                service[key] = [];
            }
        })
    }

    function logout() {
        var defer = $q.defer();
        api.get('auth/logout').then(function(response) {
            if (response === 'true') {
                // clear user data after log out.
                // just to make sure we have no left overs.
                clear();
                defer.resolve(response);
            } else {
                defer.reject(response);
            }
        });
        return defer.promise;

    }


    function user() {

        if (service._user.length !== 0) {
            return service._user.promise;
        }
        service._user = $q.defer();

        api.get('auth/user').then(function(response) {
            if (response.data.type == 'User' || response.data.type === 'Ruser') {
                // User is logged, can return the user information.
                prepareUser(response);
                service._user.resolve(response);
            } else {
                // User is not logged in, reject the promise.
                service._user.reject(response);
            }

        });
        return service._user.promise;
    }

    function authenticated() {
        var defer = $q.defer();
        if (SPAPP_CONFIG.authenticated) {
            defer.resolve(SPAPP_CONFIG.authenticated);
        } else {
            defer.reject(SPAPP_CONFIG.authenticated);
        }

        //this.user().then(function(response) {
        //    //user is authenticated return true;
        //    defer.resolve(true);
        //},function(error) {
        //    //user is not authenticated, return false
        //    defer.reject(false);
        //});
        return defer.promise;
    }

}

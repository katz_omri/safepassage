/**
 * Created by omri on 2/19/16.
 */
module.exports = ['$stateProvider', States]

function States($stateProvider) {
    $stateProvider.state('auth', {
        url: '/auth',
        abstract: true,
        templateUrl: "app/scripts/auth/view/auth.html"

    })
        .state('auth.login', {
            url: '^/login',
            controller: 'LoginController',
            controllerAs: 'auth',
            templateUrl: 'app/scripts/auth/view/auth.form.html',
            data: {
                menuIndex: 2,
                visible: false,
                name: 'Login',
                menu: 'main',
                role: 'anonymous',
                requiresLogin: false
            }

        })
        .state('auth.reset', {
            url: '^/reset',
            controller: 'LoginController',
            templateUrl: 'app/scripts/auth/view/auth.reset.html',
            controllerAs: 'auth',
            data: {
                name: "Reset Password"
            }
        })
        .state('auth.logout', {
            url: '^/logout',
            onEnter: ['$state', '$rootScope', 'authService',
                function ($state, $rootScope, authService) {
                    authService.logout().then(function () {
                        swal({
                            title: "Logged out",
                            timer: 1000,
                            type: "success",
                            showConfirmButton: false
                        });
                    }).then(function () {
                        $state.go('home.main');
                        $rootScope.$broadcast('loggedOut');
                    });
                }],
            data: {
                menuIndex: 3,
                url: '/logout',
                visible: true,
                menu: ['main', 'toolbar', 'sidenav'],
                name: "Logout",
                role: "viewer"
            },
            controllerAs: 'auth'

        })
        .state('auth.register', {
            url: '^/register',
            abstract: true,
            controller: 'RegisterController',
            controllerAs: 'register',
            templateUrl: 'app/scripts/auth/view/auth.register.html',
            resolve: {
                countries: ['api', function (api) {
                    return api.getJson('countries').then(function (response) {
                        return response.countries;
                    });
                }],
                states: ['api', function (api) {
                    return api.getJson('us_states').then(function (response) {
                        return response.states;
                    });
                }]
            }
        })
        .state('auth.soon', {
            url: '^/soon',
            data: {
                menuIndex: 3,
                visible: true,
                url: '/soon',
                menu: ['main', 'registration', 'sidenav'],
                name: 'Register',
                role: 'anonymous'
            },
            templateUrl: 'app/scripts/auth/view/auth.register.soon.html'
        })
        .state('auth.register.sponsor', {
            url: '^/register/sponsor',
            templateUrl: 'app/scripts/auth/view/auth.register.sponsor.html',
            data: {
                menuIndex: 1,
                visible: false,
                sequence: "start",
                menu: ['main', 'registration'],
                name: 'Register Draft',
                requiresLogin: false
            }
        })
        .state('auth.register.user', {
            url: '^/register/user',
            templateUrl: 'app/scripts/auth/view/auth.register.user.html',
            data: {
                menuIndex: 2,
                visible: false,
                sequence: "end",
                menu: ['registration'],
                name: 'Add User',
                requiresLogin: false
            }
        });

}
/**
 * Created by omri on 1/9/16.
 */


import LoginController    from './auth.login.controller';
import RegisterController from './auth.register.controller';
/**
 * Define the general auth controllers module.
 * Other controllers for auth will bind to this module.
 */




module.exports = angular.module('app.auth.controllers', [])
    .controller('LoginController', LoginController)
    .controller('RegisterController', RegisterController);

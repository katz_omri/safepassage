/**
 * Created by omri on 8/21/15.
 */
module.exports = [
    'menu',
    'states',
    'countries',
    '$state',
    'api',
    RegisterController
];


function RegisterController(menu, states, countries, $state, api) {
    var vm = this;
    vm.user = {};
    vm.sponsor = {};
    window.onbeforeunload = function (event) {
        var message = 'If you leave this page you are going to lose all unsaved changes, are you sure you want to leave?';
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    }
    vm.form = {};
    vm.states = states;
    vm.countries = countries;
    vm.next = next;
    vm.back = back;
    vm.submit = submit;
    vm.stepsMenu = createMenu('registration', 'anonymous');
    vm.register = register;
    vm.types = [
        {
            name: 'Sponsorship Agreement Holder',
            value: 'SAH'
        },
        {
            name: 'Community sponsor',
            value: 'CS'
        },
        {
            name: 'Group of five',
            value: 'GOF'
        },
        {
            name: 'Group of two',
            value: 'GOT'
        },
        {
            name: 'Non-profit legal person',
            value: 'NPLP'
        }
    ];

    function back(event) {
        $state.go('auth.register.sponsor');
    }

    function next(event) {
        vm.form.sponsor.$setSubmitted();
        if (vm.form.sponsor.$valid) {
            $state.go('auth.register.user');
            vm.user.country = vm.sponsor.country;
        }
    }

    function submit(event) {
        vm.form.user.$setSubmitted();
        var data = {
            'user': vm.user,
            'sponsor': vm.sponsor
        };
        api.post('sponsors', data).then(submitHandler);

    }

    function submitHandler(response) {
    }

    function register() {
    }


    function createMenu(id, role, user) {
        return menu.create({
            id: id,
            role: role,
            user: user
        });
    }

}

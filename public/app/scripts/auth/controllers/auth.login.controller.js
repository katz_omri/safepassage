/**
 * Created by omri on 8/21/15.
 */

module.exports = [
    '$state',
    'authService',
    '$rootScope',
    '$timeout',
    LoginController
];

function LoginController($state, authService, $rootScope, $timeout) {
    var vm = this;
    vm.user = {};
    vm.login = login;
    vm.reset = reset;


    function login() {
        vm.submit = true;
        authService.login(vm.user).then(function(data) {
            $rootScope.user = data;
            swal({
                title: "Logged in",
                text: "Hello " + data.data.attributes.first_name,
                timer: 1000,
                type: "success",
                showConfirmButton: false
            });
            $rootScope.$broadcast('loggedIn', data);
            $state.go('home.main');
        }, function(reason) {
            vm.error = reason.data.attributes.message;
            vm.submit = false;
        });
    };

    function reset() {
        vm.submit = true;
        authService.reset(vm.user).then(function(data) {
            vm.submit = false;
            vm.success = true;
            swal({
                title: "Password reset",
                text: "Email to reset password was sent",
                timer: 1000,
                showConfirmButton: false
            });
            $state.go('home.main');
        }, function(error) {
            console.log(error);
        })
    }


}



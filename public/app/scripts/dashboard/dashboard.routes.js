/**
 * Created by omri on 2/19/16.
 */

module.exports = [
    '$stateProvider',
    States
];



function States($stateProvider) {
    $stateProvider.state('dashboard', {
        url: '/dashboard',
        abstract: true,
        controller: 'DashboardController',
        controllerAs: 'dashboard',
        templateUrl: "app/scripts/dashboard/view/dashboard.html",
        resolve: {
            rusers: ['authService', 'userService', function(authService, userService) {
                return authService.user().then(function (user) {
                    return userService.sponsor(user).then(function(sponsor) {
                        return userService.rusers(sponsor);
                    });
                });
            }],
            users: ['authService','userService', function(authService, userService) {
                return authService.user().then(function (user) {
                    return userService.sponsor(user).then(function(sponsor) {
                        return userService.users(sponsor)
                    });
                });
            }]
        }
    })
        .state('dashboard.manage', {
            url: '^/manage',
            controller: 'DashboardController',
            controllerAs: 'dashboard',
            onEnter: ['$state', '$timeout',
                function (state, $timeout) {
                    $timeout(function() {
                        state.go('dashboard.users');
                    }, 10);
                }],
            data: {
                visible: true,
                name: 'Manage',
                menu: ['main','toolbar'],
                role: 'sponsor',
                requiresLogin: true
            }
        })
        .state('dashboard.users', {
            url: '^/manage/users',
            controller: 'DashboardController',
            controllerAs: 'controller',
            templateUrl: "app/scripts/dashboard/view/dashboard.users.html",
            data: {
                visible: true,
                name: 'Users',
                menu: 'dashboard',
                role: 'sponsor',
                requiresLogin: true
            }
        })
        .state('dashboard.refugees', {
            url: '^/manage/refugees',
            controller: 'DashboardController',
            controllerAs: 'controller',
            templateUrl: "app/scripts/dashboard/view/dashboard.rusers.html",
            data: {
                visible: true,
                name: 'Refugees',
                menu: 'dashboard',
                role: 'sponsor',
                requiresLogin: true
            }
        })
        .state('dashboard.refugees.detail', {
            url: '^/manage/refugees/:refugeeId',
            controller: 'DashboardRusersController',
            controllerAs: 'rusersController',
            templateUrl: "app/scripts/dashboard/view/dashboard.rusers.details.html",
            data: {
                visible: false
            }
        })
        .state('dashboard.users.detail', {
            url: '^/manage/users/:userId',
            controller: 'DashboardUsersController',
            controllerAs: 'usersController',
            templateUrl: "app/scripts/dashboard/view/dashboard.users.details.html",
            data: {
                visible: false
            }
        });
}


/**
 * Created by omri on 12/18/15.
 */

/**
 * Define a module to hold the services for the dashboard module.
 */

import userService from './dashboard.user.service';

module.exports = angular.module('app.dashboard.services', [])
    .service('userService', userService);

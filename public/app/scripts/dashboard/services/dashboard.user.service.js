/**
 * Created by omri on 12/18/15.
 */

module.exports = [
    '$q',
    'api',
    '$rootScope',
    'logger',
    userService
];




function userService($q, api, $rootScope, logger) {
    var _sponsor = [];
    var _users = [];
    var _rusers = [];
    var service = {
        sponsor: sponsor,
        _sponsor: _sponsor,
        users: users,
        clear: clear,
        rusers: rusers,
        _rusers: _rusers,
        _users: _users
    }
    registerListeners();
    return service;

    function registerListeners() {
        $rootScope.$on('loggedOut', function() {
            logger.info('logged out');
            clear();
        })
    }

    function clear() {
        angular.forEach(service, function(value, key) {
            if ( key.indexOf('_') !== -1 ) {
                logger.info('reset key in user service: ', key);
                service[key] = [];
            }
        })
    }
    function sponsor(user) {

        if (service._sponsor.length !== 0) {
            return service._sponsor.promise;
        }
        service._sponsor = $q.defer();
        api.get('users/' + user.data.id + '/sponsor')
            .then(function(response) {
                service._sponsor.resolve(response);
            });
        return service._sponsor.promise;
    }
    // get all users for specific sponsor.
    // get it by sponsor response object.
    function users(sponsor) {
        if (service._users.length !== 0) {
            return service._users.promise;
        }
        service._users = $q.defer();
        api.get('sponsors/' + sponsor.data.id + '/users')
            .then(function(response) {
                service._users.resolve(response);
            });
        return service._users.promise;
    }

    function rusers(sponsor) {
        if (service._rusers.length !== 0) {
            return service._rusers.promise;
        }
        service._rusers = $q.defer();
        api.get('sponsors/' + sponsor.data.id + '/rusers')
            .then(function(response) {
                service._rusers.resolve(response);
            });
        return service._rusers.promise;
    }



}



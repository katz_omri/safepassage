/**
 * Created by omri on 1/9/16
 *
 * Define the general auth controllers module.
 * Other controllers for auth will bind to this module.
 */

import DashboardController       from './dashboard.controller';
import DashboardUsersController  from './dashboard.users.controller';
import DashboardRusersController from './dashboard.rusers.controller';


module.exports = angular.module('app.dashboard.controllers', [])
    .controller('DashboardController', DashboardController)
    .controller('DashboardRusersController', DashboardRusersController)
    .controller('DashboardUsersController', DashboardUsersController);

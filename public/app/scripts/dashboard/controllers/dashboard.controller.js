/**
 * Created by omri on 12/16/15.
 */

module.exports = [
    'authService',
    'menu',
    '$state',
    'users',
    'rusers',
    DashboardController
];


function DashboardController(authService, menu, $state, users, rusers) {



    var vm = this;
    vm.goToUser = goToUser;
    vm.Menu = {};
    activate();

    function activate() {
        vm.users = users;
        vm.rusers = rusers;
        return authService.user().then(function (user) {
            user.role(user).then(function (role) {
                createMenu(role, user);
            })
        });
    }

    function createMenu(role, user) {
        vm.Menu = menu.create({
            id: 'dashboard',
            role: role,
            user: user
        });
    }

    function goToUser(type, id) {
        if (type === 'Ruser') {
            return $state.go('dashboard.refugees.detail', {refugeeId: id})
        }
        return $state.go('dashboard.users.detail', {userId: id});
    }
}




/**
 * Created by omri on 12/19/15.
 */


module.exports = [
    'rusers',
    '$stateParams',
    'utils',
    '$state',
    DashboardRusersController
]




function DashboardRusersController(rusers, $stateParams, utils, $state) {

    var vm = this;
    activate();

    function activate() {

        vm.ruser = utils.findById(rusers.data, $stateParams.refugeeId);
        if ( ! vm.ruser ) {
            // user not found, return to parent state.
            return $state.go('^');
        }
        vm.rusers = rusers;
        vm.loginData = utils.findById(rusers.included, vm.ruser.relationships.loginData.data.id);
    }

}


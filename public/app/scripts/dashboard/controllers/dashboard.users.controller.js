/**
 * Created by omri on 12/19/15.
 */


module.exports = [
    'users',
    '$state',
    'utils',
    '$stateParams',
    DashboardUsersController
]




function DashboardUsersController(users, $state, utils, $stateParams) {

    var vm = this;
    activate();


    function activate() {
        vm.user = utils.findById(users.data, $stateParams.userId);
        if ( ! vm.user ) {
            // user not found, return to parent state.
            return $state.go('^');
        }
        vm.loginData = utils.findById(users.included, vm.user.relationships.loginData.data.id);

    }


}




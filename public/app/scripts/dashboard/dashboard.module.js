/**
 * Created by omri on 12/16/15.
 */


import './services/dashboard.services.module';
import './controllers/dashboard.controllers.module';
import routes from './dashboard.routes';


angular.module('app.dashboard', [
    'app.dashboard.services',
    'app.dashboard.controllers'
])
    .config(routes);


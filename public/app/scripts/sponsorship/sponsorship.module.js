/**
 * Created by omri on 2/19/16.
 */

import routes  from './sponsorship.routes';
import controllers  from './controllers/sponsorship.controllers.module';

angular.module('app.sponsorship', [
    'app.sponsorship.controllers'
])
    .config(routes)

/**
 * Created by omri on 2/19/16.
 */

module.exports = [
    '$stateProvider',
    States
];

function States($stateProvider) {
    $stateProvider.state('sponsor', {
            url: '/sponsor',
            controller: 'HomeController',
            controllerAs: 'home',
            category: 'sponsor',
            templateUrl: 'app/scripts/home/view/sponsor.html',
            data: {
                menuIndex: 1,
                name: 'Sponsor',
                visible: false,
                menu: 'main',
                requiresLogin: false
            }
        }
    );
}

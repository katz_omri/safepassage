import $ from 'jquery';

module.exports = [
    '$httpProvider',
    '$urlRouterProvider',
    '$provide',
    'loggerProvider',
    '$locationProvider',
    'SPAPP_CONFIG',
    '$uiViewScrollProvider',
    configure
];



function configure ($httpProvider,
                    $urlRouterProvider,
                    $provide,
                    loggerProvider,
                    $locationProvider,
                    SPAPP_CONFIG,
                    $uiViewScrollProvider) {
    // turn debugging off/on (no info or warn)
    loggerProvider.debugEnabled(SPAPP_CONFIG.debug);
    $locationProvider.html5Mode(false);
    $uiViewScrollProvider.useAnchorScroll()
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.post['X-XSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

    //var modifiedGreyMap = $mdThemingProvider.extendPalette('grey', {
    //    '500': 'f6f6f6'
    //
    //});
    //var altGreyPalette = $mdThemingProvider.extendPalette('grey', {
    //    '500': '03A9F4',
    //    '50': '03A9F4',
    //    'A400': '03A9F4',
    //    'A700': '03A9F4'
    //
    //});
    //$mdThemingProvider.theme('altTheme')
    //    .primaryPalette('altGreyPalette');
    //
    //$mdThemingProvider.definePalette('modifiedGreyMap', modifiedGreyMap);
    //$mdThemingProvider.definePalette('altGreyPalette', altGreyPalette);
    //$mdThemingProvider.theme('default')
    //    .primaryPalette('modifiedGreyMap')
    //.accentPalette('cyan')
    //.warnPalette('cyan')

    // For any unmatched url, send to /route1
    $urlRouterProvider.otherwise("/");
}

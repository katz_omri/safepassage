/**
 * Created by omri on 2/19/16.
 */

import run                from './run';
import config             from './config';
import constants          from './constants';

import apiFactory         from './services/api.service';
import wowFactory         from './services/wow.service';
import postsLimit         from './filters/postsLimit';
import postsFilter        from './filters/postsFilter';
import Controllers        from './controllers/core.controllers.module';
import menuFactory        from './services/menu.factory';
import utilsFactory       from './services/utils.service';
import cacheService       from './services/cache.service';
import loggerService      from './services/logger.service';
import toArrayFilter      from './filters/toArrayFilter';
import loggerProvider     from './services/logger.provider';



angular.module('app.core', [
    'controllers',
    'ngAnimate',
    'ui.router',
    'ngResource',
    'LocalStorageModule'
])
    .config(config)
    .constant('SPAPP_CONFIG', constants)
    .factory('api', apiFactory)
    .factory('menu', menuFactory)
    .factory('utils', utilsFactory)
    .factory('wowService', wowFactory)
    .service('cache', cacheService)
    .service('logger', loggerService)
    .provider('logger', loggerProvider)
    .filter('postsFilter', postsFilter)
    .filter( 'postsLimit', postsLimit)
    .filter( 'toArrayFilter', toArrayFilter)
    .run(run);
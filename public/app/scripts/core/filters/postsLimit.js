/**
 * Created by omri on 8/15/15.
 *
 */
module.exports = [
    postsLimit
];


function postsLimit() {
    return function (content, limit, offset) {
        offset = offset || 0;
        if (limit === 'all') {
            return Array.prototype.slice.call(content, offset);
        }
        return Array.prototype.slice.call(content, offset, offset + limit);
    }
}


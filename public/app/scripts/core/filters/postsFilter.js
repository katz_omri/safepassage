/**
 * Created by omri on 8/15/15.
 *
 */
module.exports = [
    postsFilter
];


function postsFilter() {
    return function (content, category, categories) {
        category = getCategoryId(category, categories);
        return Array.prototype.filter.call(content, function(item) {
            var found = false;
            angular.forEach(item.relationships.category.data, function(value, key) {
                found = value.id === category;
            });
            return found;
        })
    }

}
function getCategoryId(category, categories) {
    var id = null;
    angular.forEach(categories, function(value, key) {
        if (value.attributes.name === category) {
            id = value.id;
        }
    });
    return id;

}
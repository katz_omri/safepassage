/**
 * Created by omri on 12/26/15.
 */

/**
 * @type Factory
 */
module.exports = [utils];


function utils() {
    return {
        // Util for finding an object by its 'id' property among an array
        findById: function findById(a, id) {

            for (var i = 0; i < a.length; i++) {
                if (a[i].id == id) return a[i];
            }
            return null;
        },
        isEmpty: function(obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop))
                    return false;
            }

            return true;
        }
    };
}

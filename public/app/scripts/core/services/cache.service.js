/**
 * Created by omri on 12/13/15.
 */

/**
 *
 * @type Service
 */
module.exports = [
    'logger',
    'localStorageService',
    '$q',
    cache
];

/**@type Service
 * @namespace cache
 * @desc Application cache
 */
function cache(logger, localStorageService, $q) {

    var excluded = [
        'auth/user',
        'auth/logout'
    ];
    var expiry_time = 5;

    var service = {
        get: get,
        set: set,
        keys: keys,
        excluded: getExcluded,
        addExclude: addExclude,
        setExpiry: setExpiry,
        expired: expired,
        expiry_time: expiry_time,
        removeExclude: removeExclude,
        remove: remove,
        exists: exists,
        clearAll: clearAll
    };

    return service;

    function getExcluded() {
        return excluded;
    }
    function addExclude(resource) {
        if (excluded.indexOf(resource) === -1) {
            excluded.push(resource);
        }
        return service;
    }
    function removeExclude(resource) {
        var pos = excluded.indexOf(resource);
        if ( pos === -1) {
            logger.info(resource + ' is not in the excluded list');
            return false;
        }
        logger.info('removing exclude ' + resource);
        excluded.splice(pos, 1);
        return service;
    }

    /**
     * @name expired
     * @desc check if a key is expired
     * @param {String} key
     * @returns {Boolean}
     * @memberOf Factories.Logger
     */

    function expired(key) {
        var value = localStorageService.get(key);
        var currentTime = new Date();
        var cacheTime = new Date(0);
        cacheTime.setUTCSeconds(value.expire);
        return currentTime >= cacheTime;

    }

    /**
     * @name get
     * @desc get key from cache
     * @param {String} key
     * @returns {Promise}
     * @memberOf Factories.Logger
     */
    function get(key) {
        var defer = $q.defer();
        var value = localStorageService.get(key);
        if (value === null) {
            defer.reject("key not found");
        } else {
            logger.info('returning resource: ' + key + ' from cache!');
            defer.resolve(value);
        }
        return defer.promise;
    }


    function remove(keys) {
        var defer = $q.defer();
        if (typeof keys === "string") {
            defer.resolve(localStorageService.remove(keys));
            return defer.promise;
        }
        keys.forEach(function(key) {
            defer.resolve(localStorageService.remove(key));
        })
        return defer.promise;
    }

    /**
     * @name set
     * @desc set key in cache
     * @param {String} key
     * @param {Mixed} value
     * @returns {Promise}
     * @memberOf core.services
     */
    function set(key, value) {
        var defer = $q.defer();
        if (_ignore(key)) {
            logger.info(key + ' is in the excluded list, not serving from cache');
            defer.reject(value);
        } else {
            logger.info('resource: ' + key + ' was added to cache!' );
            service.setExpiry(value);
            localStorageService.set(key, value);
            defer.resolve(value);
        }
        return defer.promise;
    };

    function setExpiry(resource) {
        var expiry = service.expiry_time * 60;
        var time = parseInt(new Date() / 1000);
        logger.info('expiration date was set: ' + time + expiry );
        return resource.expire = time + expiry;
    }

    function _ignore(key) {
        return (excluded.indexOf(key) === -1) ? false : true;
    }

    /**
     * @name exists
     * @desc Check if key exists in cache
     * @param {String} key to check in cache
     * @returns {boolean}
     * @memberOf
     */
    function exists(key) {
        return localStorageService.get(key) ? true : false;
    };


    /**
     * @name keys
     * @desc returns keys in cache
     * @returns {Promise}
     * @memberOf Factories.Logger
     */
    function keys() {
        var defer = $q.defer();
        defer.resolve(localStorageService.keys());
        return defer.promise;
    }
    /**
     * @name clearAll
     * @desc Clear all cache
     * @returns {Promise}
     * @memberOf Factories.Logger
     */
    function clearAll() {
        var defer = $q.defer();
        defer.resolve(localStorageService.clearAll());
        defer.promise;
    }


}
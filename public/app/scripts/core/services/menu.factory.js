/**
 *
 * @type Factory
 */
module.exports = [
    '$q',
    '$rootScope',
    'logger',
    menu
];


function menu($q, $rootScope, logger) {
    var _items = [];

    var service = {
        menus: [],
        listeners: [],
        getById: getById,
        create: create
    }


    registerListeners({
        'loggedIn': function (e, user) {
            user.role(user).then(function (role) {
                angular.forEach(service.menus, function (menu) {
                    menu.setAuthState(role, user);
                });
            }); 
        },
        'loggedOut': function () {

            angular.forEach(service.menus, function (menu) {
                menu.user = '';
                menu.setAuthState('anonymous');
            });
        }
    });


    return service;

    function registerListeners(listeners) {
        angular.forEach(listeners, function (value, key) {
            $rootScope.$on(key, value);
            logger.info('Registered listener: ', key);
        });
    }


    function getById(id) {
        var result = {};
        result.found = false;
        angular.forEach(service.menus, function (menu) {
            if (menu.id === id) {
                result = menu;
                result.found = true;
            }
        });
        return result;


    }

    // get a config object with:
    // id, user, role and requested states.
    function create(config) {
        var result = getById(config.id);
        if (result.found) {
            logger.info('menu already exists, returning: ', result);
            return result;
        }

        var menu = new Menu({
            id: config.id,
            states: angular.copy($rootScope.$state.get()),
            role: config.role,
            user: config.user
        });
        logger.info('created menu: ', menu);
        service.menus.push(menu);
        return menu;

    }


}


function Menu(config) {

    var states = config.states,
        role = config.role,
        user = config.user;
    this.id = config.id;
    this.items = [];
    this._items = [];
    this.active = true;
    this.user = config.user || '';
    this.init(states, role, user);
}

Menu.prototype = {
    init: function (states, role, user) {
        this._items = _createItemsList(states, this.id);
        this.setAuthState(role, user);
    },
    setAuthState: function (role, user) {
        var roles = {};
        roles["anonymous"] = 0;
        roles["viewer"] = 1;
        roles["refugee"] = 2;
        roles["sponsor"] = 3;
        roles["admin"] = 4;
        this.items = this._items.filter(function (item) {
            // if state allows all roles keep it in the menu.
            if (item.role === 'all') {
                return true;
            }

            // if logged in and menu item require anonymous role, remove it from the menu.
            // meaning, states that want to remove themselves after login, will set their role requirement to anonymous.
            if (item.role === 'anonymous' && role !== 'anonymous') {
                return false;
            }

            //if logged in user role is bigger than the menu item role, show it.
            if (roles[item.role] <= roles[role]) {
                return true;
            }

        });
        if (typeof(user) !== 'undefined') {
            this.user = user.data.attributes.first_name;
        }
    }
}

function _assignMenu(id, items) {
    return items.filter(function (item) {
        if (angular.isString(item.menus) && item.menus === id) {
            return true;
        }
        if (angular.isArray(item.menus)) {
            for (var i = 0; i < item.menus.length; i++) {
                if (item.menus[i] === id) {
                    return true;
                }

            }

        }

        return false;
    });

}

function _createItemsList(states, id) {
    var items = [];
    states.map(function (state) {
        var item = {};

        if ('data' in state && state.data.visible) {

            item.name = state.data.name;
            item.menus = state.data.menu;
            item.visible = state.data.visible;
            item.route = state.name;
            // if role requirement is not set, I assume the state is allowed for all roles.
            item.role = state.data.role || "all";
            item.url = '#';
            item.url += state.data.url || state.url;

            item.index = state.data.menuIndex || 0;
            item.requiresLogin = state.data.requiresLogin;
            items.push(item);
        }
    });

    items = _assignMenu(id, items);

    return items;
}


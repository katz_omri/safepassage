module.exports = ['$log', 'logProvider', logger];

/**
 * @type Service
 * @namespace Logger
 * @desc Application wide logger
 * @memberOf Factories
 */
function logger($log, logProvider) {
    var mockService = {
        error: function(){},
        log: function(){},
        info: function(){},
        debug: function(){},
        warn: function(){}
    }
    var service = {
        error: error,
        log: log,
        info: info,
        debug: debug,
        warn: warn
    };

    return $log.debugEnabled ? service : mockService;


    /**
     * @name debug
     * @desc Logs debug notices
     * @param {String} msg Message to log
     * @returns {String}
     * @memberOf Factories.Logger
     */
    function debug(msg, extra) {
        extra ? $log.debug(msg, extra) : $log.debug(msg);
        return msg;
    };

    /**
     * @name error
     * @desc Logs errors
     * @param {String} msg Message to log
     * @returns {String}
     * @memberOf Factories.Logger
     */
    function error(msg, extra) {
        extra ? $log.error(msg, extra) : $log.error(msg);
        return msg;
    };
    /**
     * @name log
     * @desc Logs errors
     * @param {String} msg Message to log
     * @returns {String}
     * @memberOf Factories.Logger
     */
    function log(msg, extra) {

        extra ? $log.log(msg, extra) : $log.log(msg);
        return msg;
    }
    /**
     * @name info
     * @desc Info logger
     * @param {String} msg Message to log
     * @returns {String}
     * @memberOf Factories.Logger
     */
    function info(msg, extra) {
        extra ? $log.info(msg, extra) : $log.info(msg);
        return msg;
        //console.log(logProvider);
        //var args = [];
        //angular.forEach(arguments, function(arg) {
        //    args.push(arg);
        //});
        //for(var i = 0; i < 2; i++) {
        //    if (i % 2 === 0) {
        //        args[i] = '%c' + args[i] ;
        //    } else {
        //        args.splice(i , 0,'background: #222; color: #4CAF50');
        //    }
        //}


        //return msg;
    }



    /**
     * @name warn
     * @desc Logs warnings
     * @param {String} msg Message to log
     * @returns {String}
     * @memberOf Factories.Logger
     */

    function warn(msg, extra) {
        extra ? $log.warn(msg, extra) : $log.warn(msg);
        return msg;
    };

}

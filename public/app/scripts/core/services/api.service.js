/**
 * Created by omri on 12/13/15.
 */

/**
 *
 * @type Factory
 */
module.exports = [
    '$http',
    '$rootScope',
    '$q',
    '$resource',
    'cache',
    'logger',
    api
];


function api($http, $rootScope, $q, $resource, cache, logger) {

    var _posts = [],
        _category = [],
        fetching = false,
        service = {
            'get': get,
            'post': post,
            'fetching': fetching,
            'posts': posts,
            'sendMail': sendMail,
            'getJson': getJson,
            '_posts': _posts,
            'category': category,
            '_category': _category
        };

    return service;

    function get(resourceUrl, disableCache, type, callback) {
        service.fetching = true;

        logger.info('performing get request');
        if (cache.exists(resourceUrl) && !cache.expired(resourceUrl) && !disableCache) {
            service.fetching = false;
            return cache.get(resourceUrl);
        }
        var skipAuth = (type !== undefined && type !== 'Secured') ? true : false;
        return $http({
            url:  'api/v1/' + resourceUrl,
            method: 'GET',
            skipAuthorization: skipAuth,
            crossDomain: true // enable this
        }).then(function(response) {
            _cache(resourceUrl, response.data);
            logger.debug(resourceUrl, response.data);
            logger.info('get request data: ', response.data);
            service.fetching = false;
            $rootScope.$broadcast('getRequest', response);
            return response.data;
        });
    }

    function _cache(resourceUrl, data) {
        cache.set(resourceUrl, data);
    }

    function post(resourceUrl, data, type) {
        service.fetching = true;
        logger.info('performing post request');
        logger.info('post request data: ', data);
        var skipAuth = (type !== undefined && type !== 'Secured') ? true : false;
        return $http({
            url: 'api/v1/' + resourceUrl,
            method: 'POST',
            skipAuthorization: skipAuth,
            data: data
        }).then(function (response) {
            service.fetching = false;
            cache.clearAll();
            return response.data;
        });
    }

    function posts(category, disableCache) {
        if (category) {
            return postsForCategory(category, disableCache);
        }
        service._posts = $q.defer();
        service.get('posts', disableCache)
            .then(function(posts) {
                service._posts.resolve(posts);
            });
        return service._posts.promise;

    }

    function postsForCategory(category, disableCache) {
        return service.category(category, disableCache).then(function(category) {
            return handleCategoryResponse(category, disableCache);
        });
    }

    function sendMail(data) {
        var defer = $q.defer();
        service.post('mail/send', data)
            .then(function(response) {
                logger.info('sending mail with ', data);

                if (response.data.type  === 'Mail') {
                    return defer.resolve(response);
                }
                if (response.data.type === 'ValidationException') {
                    logger.error('Error validating email fields', response);
                    defer.reject(response);
                }
            }, function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    function handleCategoryResponse(response, disableCache) {
        var category_id = response.data.id;
        return service.get('categories/' + category_id + '/posts', disableCache)
            .then(function(response) {
                return response;
            });
    }


    function category(name, disableCache) {
        logger.info('fetching category :' + name);
        return this.get('categories?name=' + name, disableCache);
    }

    function getJson(model) {
        logger.info('fetching json model');
        if (cache.exists(model) && !cache.expired(model)) {
            logger.info('json model ' + model + ' already in cache');
            return cache.get(model);
        }
        // get Json model from local json files.
        var defer = $q.defer();
        var promise = $resource('app/scripts/models/:modelName.json', {}, {
            query: {method:'GET', params:{modelName:model}, isArray:false}
        });
        promise.query().$get({modelName: model}).then(function(response) {
            _cache(model, response);
            logger.info('return json model response ', response);
            defer.resolve(response);
        })
        return defer.promise;
    }

    function _sanitizeResourceUrl(url) {
        logger.log('sanitizing url ', url);
        return url.replace(/^\//, "");
    }

}

/**
 * Created by omri on 12/14/15.
 */

/**
 * @type Factory
 */
module.exports = [
    wowService
];



    function wowService() {
        var instances = [];

        var service = {
            instances: instances,
            init: init
        }

        function init() {
            return new WOW().init();
        }
        return service;
    }



/**
 * Created by omri on 4/29/16.
 */

module.exports = [
    '$rootScope',
    '$state',
    '$stateParams',
    'authService',
    Run
];


function Run($rootScope, $state, $stateParams, authService) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(e, to) {
        if ( typeof(to.data.autoScroll) !== 'undefined' && ! to.data.autoScroll) {
            return;
        }
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    //$rootScope.$on('$stateChangeStart', function(e, to) {
    //    if (to.data && to.data.requiresLogin) {
    //        authService.user().then(function(user) {
    //        }, function(error) {
    //            $state.go('auth.login');
    //        })
    //    }
    //});
}

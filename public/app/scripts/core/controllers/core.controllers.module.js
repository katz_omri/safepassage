/**
 * Created by omri on 12/16/15.
 */


import MainController from './core.main.controller';


angular.module('controllers', [])
    .controller('MainController', MainController)

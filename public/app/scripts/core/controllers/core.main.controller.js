module.exports = [
    '$rootScope',
    'authService',
    'menu',
    '$scope',
    'logger',
    'SPAPP_CONFIG',
    MainController
];


function MainController($rootScope, authService, menu, $scope, logger, SPAPP_CONFIG) {
    var vm = this;
    vm.loggedIn = false;
    vm.authenticating = false;
    vm.toggleMenu = toggleMenu;
    vm.closeMenu = closeMenu;
    vm.Menu = {};
    vm.Toolbar = {};
    vm.Sidenav = {};

    activate();

    function closeMenu() {
        logger.info('closing main menu');
        //$mdSidenav('left').close();
    }
    function toggleMenu() {
        logger.info('toggeling menu');
        //$mdSidenav('left').toggle();
    }

    function activate() {
        logger.info('MainController activated');
        registerListeners();
        if (menu.active) {
            return;
        }
        if ( ! SPAPP_CONFIG.authenticated) {
            vm.Menu = createMenu('main', 'anonymous');
            vm.Sidenav = createMenu('sidenav', 'anonymous');
            return ;
        }


        return authService.user().then(function (user) {
            vm.user = user;
            user.role(user).then(function (role) {
                vm.Menu = createMenu('main', role, user);
                vm.Toolbar = createMenu('toolbar', role, user);
                vm.Sidenav = createMenu('sidenav', role, user);
            })
        }, function () {
            // no user was found, create anonymous menu.
            vm.Menu = createMenu('main', 'anonymous');
            vm.Sidenav = createMenu('sidenav', 'anonymous');
        });

    }


    function registerListeners() {
        logger.info('adding event listener to loggedIn event');
        $rootScope.$on('loggedIn', function(e, user) {
            user.role(user).then(function (role) {
                vm.Toolbar = createMenu('toolbar', role, user);
            });
        });
    }

    function createMenu(id, role, user) {

        return menu.create({
            id: id,
            role: role,
            user: user
        });

    }
}


/**
 * Created by omri on 8/15/15.
 *
 */
import HomeControllers from './controllers/home.controllers.module';
import routes from './home.routes';

angular.module('app.home', [
    'app.home.controllers'
])
    .config(routes)
    .filter('contentFilter', [contentFilter])
    .filter('postsFilter', [postsFilter])

    .filter('contentLimit', [contentLimit]);
function contentLimit() {
    return function (content, limit, offset) {
        offset = offset || 0;
        if (limit === 'all') {
            return Array.prototype.slice.call(content, offset);
        }
        return Array.prototype.slice.call(content, offset, offset + limit);
    }
}
function postsFilter() {
    return function (content, category, categories) {
        category = getCategoryId(category, categories);
        return Array.prototype.filter.call(content, function(item) {
            var found = false;
            angular.forEach(item.relationships.category.data, function(value, key) {
                found = value.id === category;
            });
            return found;
        })
    }

}
function getCategoryId(category, categories) {
    var id = null;
    angular.forEach(categories, function(value, key) {
        if (value.attributes.name === category) {
            id = value.id;
        }
    });
    return id;

}
function contentFilter() {


    return function (content, groupId, contentType) {
        if (typeof(content) === 'undefined') {
            return;
        }
        var posFilter = Array.prototype.filter.call(content, function (item) {
            if (typeof (contentType) !== 'undefined') {
                // allow to filter by type of content
                if (item.attributes.type !== contentType) {
                    return false;
                }
            }
            return (item.attributes.group_id === groupId);
        });
        return posFilter;
    };
}


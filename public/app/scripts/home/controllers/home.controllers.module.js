/**
 * Created by omri on 1/9/16.
 */

import HomeController    from './home.controller';
/**
 * Define the general auth controllers module.
 * Other controllers for auth will bind to this module.
 */




module.exports = angular.module('app.home.controllers', [])
    .controller('HomeController', HomeController);

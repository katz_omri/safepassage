/**
 * Created by omri on 8/21/15.
 */

module.exports = [
    'features',
    'logger',
    HomeController
];



function HomeController(features, logger) {
    var vm = this;
    vm.cards = {};
    vm.posts = {};
    vm.features = {};
    vm.loaded = false;
    vm.loggedIn = false;
    vm.fetched = false;

    vm.authenticating = false;
    activate();

    function activate() {
        logger.info('HomeController activated');
        vm.features = features;
        logger.info('setting features: ', features);
    }


}

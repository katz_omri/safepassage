/**
 * Created by omri on 2/19/16.
 */

module.exports = [
    '$stateProvider',
    States
]
function States ($stateProvider) {
    $stateProvider.state('home', {
        url: '/',
        controller: 'HomeController',
        abstract: true,
        controllerAs: 'home',
        templateUrl: 'app/scripts/home/home.html',
        resolve: {
            features: ['api','logger', function (api, logger) {
                return api.getJson('features').then(function(response) {
                    logger.info('resolving home.features: ', response.features);
                    return response.features;
                });
            }]
        }

    })
        .state('home.main', {
            url: '',
            category: 'home',
            data: {
                menuIndex: 1,
                visible: true,
                url: '/',
                name: 'Home',
                menu: 'sidenav',
                requiresLogin: false
            }
        })
        .state('home.contact', {
            url: '^/contact',
            resolve: {
            },
            templateUrl: 'app/scripts/home/view/partials/contact.html',
            data: {
                visible: false
            }
        })
}
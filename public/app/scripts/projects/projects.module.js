/**
 * Created by omri on 2/19/16.
 */

import routes              from './projects.routes';
import ProjectsControllers from './controllers/projects.controllers.module';

angular.module('app.projects', [
    'app.projects.controllers'
])
    .config(routes);

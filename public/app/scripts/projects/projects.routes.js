/**
 * Created by omri on 2/19/16.
 */

module.exports = [
    '$stateProvider',
    States
];
function States($stateProvider) {
    $stateProvider.state('projects', {
        url: '/projects',
        templateUrl: 'app/scripts/projects/projects.html',
        controller: 'ProjectsController',
        controllerAs: 'projects',
        category: 'projects',
        abstract: true,
        data: {
            visible: false
        }
    })
        .state('projects.main', {
            url: '',
            resolve: {
            },
            data: {
                menuIndex: 3,
                visible: true,
                url: '/projects',
                name: 'Projects',
                menu: ['main', 'sidenav'],
                requiresLogin: false
            }
        });
}
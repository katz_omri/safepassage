/**
 * Created by omri on 2/19/16.
 */

module.exports = [
    'api',
    ProjectsController
];



function ProjectsController(api) {

    var vm = this;
    vm.fetched = false;
    vm.posts = {};
    vm.categories = {};

    activate();
    function activate() {

        //vm.posts = posts;
        //vm.categories = categories;
        initCards();
        vm.apiFetching = api.fetching;
        vm.posts = _getPosts();
        vm.categories = _getCategories();
    }

    function _getPosts() {
        return api.posts('projects')
            .then(function(posts) {

                return posts.data;
            });
    }
    function _getCategories() {
        return api.posts('about')
            .then(function(posts) {
                return posts.included;
            });
    }

    function initCards() {
        var promise = api.getJson("cards");
        promise.then(function(response) {
            vm.cards = response;
        });
    }


}


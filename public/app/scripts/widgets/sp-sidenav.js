/**
 * Created by omri on 1/9/16.
 */

module.exports = [
    spSidenav
];

function spSidenav() {
    var directive = {
        replace: true,
        templateUrl: 'app/scripts/widgets/sp-sidenav.html',
        restrict: 'E'
    };
    return directive;


}

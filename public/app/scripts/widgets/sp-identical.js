/**
 * Created by omri on 3/12/16.
 */
module.exports = [
    spIdentical
];


function spIdentical() {
    var directive = {
        link: link,
        require: "?ngModel",
        scope: {
            'spIdentical': "="
        },
        restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs, ngModel) {
        ngModel.$validators.identical = function(modelValue, viewValue) {
            if (ngModel.$isEmpty(modelValue)) {
                // consider empty models to be valid
                return false;
            }
            if (scope.spIdentical === modelValue) {
                return true;
            }
            // it is invalid
            return false;
        };
    }
}


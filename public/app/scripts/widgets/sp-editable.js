/**
 * Created by omri on 2/20/16.
 */

module.exports = [
    '$state',
    'logger',
    'authService',
    'api',
    '$stateParams',
    'utils',
    spEditable
];

function spEditable($state, logger, authService, api, $stateParams, utils) {


    var directive = {
        link: link,
        require: ['?ngModel'],
        transclude: true,
        templateUrl: 'app/scripts/widgets/sp-editable.html',
        scope: {
            item: '='
        },
        controllerAs: 'vm',
        bindToController: true,
        controller: controller,
        restrict: 'E'
    };

    return directive;

    function controller() {
        var vm = this;
        vm.editMode = false;
        var panes = vm.panes = [];
        vm.isAdmin = false;
        vm.post = {};
        vm.select = select;
        vm.addPane = addPane;
        vm.paneActive = paneActive;
        vm.selectByName = selectByName;

        vm.savePost = savePost;

        activate();


        function activate() {
            authService.isAdmin().then(function() {
                vm.isAdmin = true;
            }, function(error) {
                vm.isAdmin = false;
            });
            logger.log('posts.EditController: activated');
        }
        function paneActive(name) {
            var active = false;
            angular.forEach(panes, function(pane) {
                if (pane.name === name && pane.selected) {
                    active = true;
                }
            })
            return active;
        }


        function addPane(pane) {
            if (panes.length === 0) {
                vm.select(pane);
            }
            panes.push(pane);
        }
        function selectByName(name) {
            if (name === 'edit_tab') {
                vm.editMode = true;
            }
            angular.forEach(panes, function(pane) {
                if (pane.name === name) {
                    vm.select(pane);
                }
            });

        }

        function select(pane) {
            angular.forEach(panes, function(pane) {
                pane.selected = false;
            });
            pane.selected = true;
        }

        function savePost() {
            var content = tinymce.get('editor_' + vm.item.id).getContent({format: 'raw'});
            api.post('posts/' + vm.item.id, {
                data: content,
                title: vm.item.attributes.title
            }).then(function(response) {
                vm.item = response.data;
                vm.selectByName('content_tab');
            })
        }

    }
    function link(scope, element, attrs) {



    }

}

/**
 * Created by omri on 8/15/15.
 *
 * Global directives to be used all over the app.
 */
module.exports = [
    spEnter
];

function spEnter() {
    var directive = {
        link: link,
        restrict: 'A'
    };
    return directive;

    function link(scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                console.log(event);
                scope.$apply(function (){
                    scope.$eval(attrs.spEnter);
                });

                event.preventDefault();
            }
        });
    }
}

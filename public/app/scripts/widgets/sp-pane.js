/**
 * Created by omri on 2/20/16.
 */

module.exports = [
    'logger',
    spPane,
];

function spPane(logger) {
    var directive = {
        link: link,
        require: '^^spEditable',
        transclude: true,
        template: '<div ng-show="selected" ng-transclude> </div>',
        scope: {
            name: '@'
        },
        controller: controller,
        restrict: 'E'
    };
    return directive;

    function controller() {
    }
    function link(scope, element, attrs, editableController) {
        var button;

        //element.parent().after(button);
        //handleClick = handleClick.bind(element);
        addListeners();

        editableController.addPane(scope);
        if (scope.name === 'edit_tab') {
            scope.$watch('selected', function(newValue) {
                if (newValue) {
                }
            });
        }


        function addListeners() {
            element.on('mouseenter', handleMouseEnter);
            element.on('mouseleave', handleMouseLeave);
        }
        function handleMouseEnter() {
            console.log('mouse enter');
            //button.css('display', 'block');
            //requestAnimationFrame(animateEnter);
            logger.log('mouse entered');
        }

        function animateEnter() {
            button[0].animate([
                { transform: 'scale(0)', opacity: 0, offset: 0 },
                { transform: 'scale(.2)', opacity: .2, offset: .3 },
                { transform: 'scale(.6)', opacity: .6, offset: .7875 },
                { transform: 'scale(1)', opacity: 1, offset: 1 }
            ], {
                duration: 700, //milliseconds
                easing: 'ease-in-out', //'linear', a bezier curve, etc.
                delay: 10, //milliseconds
                iterations: 1, //or a number
                direction: 'normal', //'normal', 'reverse', etc.
                fill: 'forwards' //'backwards', 'both', 'none', 'auto'
            });
        }

    }



    function handleMouseLeave() {

    }



}

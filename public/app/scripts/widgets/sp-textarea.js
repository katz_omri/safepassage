/**
 * Created by omri on 2/20/16.
 */

module.exports = [
    'logger',
    spTextarea
];

function spTextarea(logger) {
    var editor;
    var directive = {
        link: link,
        scope: {
            spData: '=',
            isAdmin: '=',
            enabled: '='
        },
        controller: controller,
        restrict: 'E'
    };
    return directive;

    function controller() {
        var vm = this;
        vm.enableEditMode = enableEditMode;
        function enableEditMode() {
            initEditor();
        }


    }



    function link(scope, element, attrs) {

        scope.$watch('enabled', function(newValue) {
            if (newValue) {
                initEditor();
            }
        })
        attrs.$set('id', 'editor_' + scope.spData.id);
        //initEditor();

        logger.log('spTextarea directive linked');

        function initEditor() {
            tinymce.remove();

            logger.log('spTextarea: activating tinymce editor');
            tinymce.init({
                selector: '#editor_' + scope.spData.id,
                height: 400,
                plugins: "code",
                content: scope.spData.attributes.content,
                menubar: "tools"
            }).then(function () {
                editor = tinymce.get('editor_' + scope.spData.id);
                editor.setContent(scope.spData.attributes.content);
            });

        }

    }

}

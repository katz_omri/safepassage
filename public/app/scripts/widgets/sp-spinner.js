/**
 * Created by omri on 2/20/16.
 */

module.exports = [
    spSpinner
];

function spSpinner() {
    var directive = {
        link: link,
        require: '?ngModel',
        scope: {
            spSpinner: '='
        },
        controller: controller,
        restrict: 'A'
    };
    return directive;

    function controller() {
    }
    function link(scope, element, attrs) {
        var spinner = directive.spinner = document.createElement('paper-spinner');
        var maxHeight = 400;
        var spinnerSize = 60;
        spinner.active = true;
        spinner.style.margin = 'auto';
        spinner.style.top = (maxHeight / 2) + 'px';
        spinner.style.width = spinnerSize + 'px';
        spinner.style.height = spinnerSize + 'px';

        element.append(directive.spinner);
        element.css('min-height', maxHeight + 'px');
        spinner.active = true;
        scope.$watch('spSpinner', function() {
            if (  isEmpty(scope.spSpinner) ) {
                return showSpinner(spinner);
            }
            hideSpinner(spinner);
        })
    }
    function isEmpty(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    function hideSpinner(spinner) {
        spinner.active  = false;
    }
    function showSpinner(spinner) {
        spinner.active = true;
    }
}

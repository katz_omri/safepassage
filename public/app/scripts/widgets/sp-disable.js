/**
 * Created by omri on 2/20/16.
 */

module.exports = [
    spDisable
];

function spDisable() {
    var directive = {
        link: link,
        require: '?ngModel',
        scope: {
            spDisable: '='
        },
        controller: controller,
        restrict: 'A'
    };
    return directive;

    function controller() {
    }
    function link(scope, element, attrs) {
        element[0].disabled = scope.spDisable;
        scope.$root.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams, options) {
                if (typeof(toState.data.menuIndex) === 'undefined') {
                    return;
                }
                element[0].disabled = true;

                if (toState.data.menuIndex !== 1) {
                    element[0].disabled = false;
                }
            });
    }
}

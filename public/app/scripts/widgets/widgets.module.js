/**
 * Created by omri on 12/12/15.
 */

import spContactWidget from './sp-contact-widget';
import spDisable       from './sp-disable';
import spEnter         from './sp-enter';
import spHeader        from './sp-header';
import spIdentical     from './sp-identical';
import spInView        from './sp-in-view';
import spMenu          from './sp-menu';
import spSidenav       from './sp-sidenav';
import spEditable      from './sp-editable';
import spTextarea      from './sp-textarea';
import spPane          from './sp-pane';

angular.module('app.widgets', [

])
    .directive('spContactWidget', spContactWidget)
    .directive('spDisable', spDisable)
    .directive('spEnter', spEnter)
    //.directive('spHeader', spHeader)
    .directive('spIdentical', spIdentical)
    .directive('spInView', spInView)
    .directive('spEditable', spEditable)
    .directive('spTextarea', spTextarea)
    .directive('spPane', spPane)
    //.directive('spMenu', spMenu)
    //.directive('spSidenav', spSidenav);


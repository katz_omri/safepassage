/**
 * Created by omri on 1/9/16.
 */

/**
 * Created by omri on 12/12/15.
 */

module.exports = [
    'api',
    spContactWidget
];

function spContactWidget(api) {
    var directive = {
        link: link,
        replace: true,
        template: '<sp-contact-dialog class="action-buttons"></sp-contact-dialog>',
        restrict: 'E'
    };
    return directive;

    function link(scope, element, attrs) {
        element[0].addEventListener('form-submitted', formSubmitted);
    }

    function formSubmitted(e) {
        api.sendMail(e.detail).then(function () {
            e.target.success();

        }, function (errors) {
            e.target.errors = errors.data.attributes.message;
        });
    }

}
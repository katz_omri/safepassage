
@push('scripts-production')

@if ($env !== 'local')
    <script src="./app/bower_components/web-animations-js/web-animations.min.js"></script>
    <script src="<% elixir('js/vendor.js') %>"></script>
    <script src="<% elixir('js/common.js') %>" async defer></script>
    <!-- <script src="./app/bower_components/tinymce/tinymce.js"></script> -->
@endif
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCaptchaCallback&render=explicit" async defer></script>

@endpush


@push('scripts-local')
    <script src="./app/bower_components/web-animations-js/web-animations.min.js"></script>
    <!-- <script src="./app/bower_components/tinymce/tinymce.min.js"></script> -->
    <script src="build/js/vendor.js"></script>
    <script src="build/js/common.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCaptchaCallback&render=explicit" async defer></script>

@endpush




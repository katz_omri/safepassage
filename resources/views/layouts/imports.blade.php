@push('imports-production')
    <!-- build:css styles/main.css -->
    <link rel="stylesheet" type="text/css" href="app/bower_components/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="app/bower_components/angular-material/angular-material.min.css">
    @if ($env !== 'local')
        <link rel="stylesheet" href="<% elixir('css/main.min.css') %>">
    @endif
    <link href='https://fonts.googleapis.com/css?family=Lato:300,100|Roboto:400,100,300|Source+Sans+Pro:400,600|Montserrat|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="app/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="app/bower_components/animate.css/animate.min.css">

    <!-- will be replaced with elements/elements.vulcanized.html -->
    <link rel="import" href="build/elements/elements.vulcanized.html">
@endpush


@push('imports-local')
    <!-- build:css styles/main.css -->
    <link rel="stylesheet" type="text/css" href="app/bower_components/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="app/bower_components/angular-material/angular-material.css">
    <link rel="stylesheet" href="build/styles/main.css">
    <!-- <link rel="stylesheet" href="app/styles/bootstrap/css/bootstrap.css"> -->
    <link rel="stylesheet" href="app/bower_components/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="app/bower_components/animate.css/animate.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,100|Roboto:400,100,300|Source+Sans+Pro:400,600|Montserrat|Fira+Sans:400,400italic' rel='stylesheet' type='text/css'>

    <!-- will be replaced with elements/elements.vulcanized.html -->
    <link rel="import" href="build/elements/elements.vulcanized.html">
@endpush
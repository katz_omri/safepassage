@push('global-config')
<script>

    SPAPP = {
        debug: <% json_encode(config('app.debug')) %>,
        env: '<% config('app.env') %>',
        captchaKey: '<% env('RECAPTCHA_SITE_KEY') %>',
        authenticated: <% json_encode(Auth::check())%>
    };

</script>

@endpush
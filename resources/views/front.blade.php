<!doctype html>
<html lang="" id="html" >
@include('layouts.scripts')
@include('layouts.imports')
@include('partials.analytics')
@include('config')
<head>

    <meta charset="utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="generator" content="Safepassage Website" />
    <title>Safepassage Website</title>
    <!-- Place favicon.ico in the `app/` directory -->

    <!-- Chrome for Android theme color -->
    <meta name="theme-color" content="#2E3AA1">

    <!-- Web Application Manifest -->
    <link rel="manifest" href="manifest.json">

    <!-- Tile color for Win8 -->
    <meta name="msapplication-TileColor" content="#3372DF">

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Safe Passage Canada">
    <link rel="icon" sizes="192x192" href="app/images/favicon.ico">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Safepassage Website">
    <link rel="apple-touch-icon" href="app/images/touch/apple-touch-icon.png">

    <!-- Tile icon for Win8 (144x144) -->
    <meta name="msapplication-TileImage" content="app/images/touch/ms-touch-icon-144x144-precomposed.png">


    @if ($env === 'local')
        @stack('imports-local')

    @else
        @stack('imports-production')
    @endif

    <script src="./app/bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>
    <!-- For shared styles, shared-styles.html import in elements.html -->
    <style is="custom-style" include="shared-styles"></style>
</head>

<body ng-controller="MainController as main" id="scrollable-body">
    <sp-header menu="sp-sidenav">
        <li ng-repeat="item in main.Menu.items | orderBy:'index'" ng-class="{active: $state.includes(item.route)}" item>
            <a ui-sref="{{item.route}}">
                {{item.name}}
            </a>
        </li>
    </sp-header>
    <sp-sidenav state="{{$state.current}}">
        <li ng-repeat="item in main.Sidenav.items | orderBy:'index'" ng-class="{active: $state.includes(item.route)}" item>
            <a ui-sref="{{item.route}}">
                {{item.name}}
            </a>
        </li>
    </sp-sidenav>
    <div ui-view class="main"></div>
    <div ui-view="edit"></div>
    <sp-footer></sp-footer>
    <sp-to-top></sp-to-top>

@stack('global-config')

@if ($env === 'local')
    @stack('scripts-local')

@else

    @stack('scripts-production')
@endif

@stack('google-analytics')


</body>

</html>
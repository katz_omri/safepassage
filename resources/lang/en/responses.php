<?php
/**
 * Created by PhpStorm.
 * User: omri
 * Date: 10/10/15
 * Time: 12:28
 */

return [
    '404' => 'Page not found',
    'LoginFailed' => 'Login Failed',
    'MissingParam' => 'Validation Failed',
    'ModelNotFoundException' => 'No query results for model [:service].',
    'CredentialsError' => 'Oops! These credentials do not match our records.',
    'ServiceForbidden' => 'Access to the action [:action] in [:service] service is forbidden.',
    'token_not_provided' => 'Access to the action [:action] in [:service] service requires Token.',
    'token_expired' => 'Token expired',
    'token_invalid' => 'Token is invalid',
    'user_not_found' => 'User was not found'

];
